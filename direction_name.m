function n = direction_name(d)
%DIRECTION_NAME Return the name of numerical direction

    if d == 0
        n = 'all';
    elseif d == -1
        n = 'left';
    elseif d == 1
        n = 'right';
    else
        n = 'unknown';
    end

end

