function graph_time_vs_angle_spread_e(filenames, simtitle, pulse_energy_j, settings, simid)

    emin = settings.emin;
    ebin_size = settings.ebin_size;
    n_ebins = settings.n_ebins;
    emax = emin + (n_ebins-1)*ebin_size;
    all_e = [0, emin:ebin_size:emax, inf];

    export_results = settings.export_results;
    differential = settings.differential;
    cumulative = settings.cumulative;
    round_n = settings.round;
    
    phot_ratio = 1;

    display_quads = find(settings.quads);
    
    disp(['Processing simulation ' simtitle]);
    el_chg = 1.6021766208e-19;
    m_e = 9.10938291e-31;
    
    pulse_energy = pulse_energy_j / el_chg / 1e6; % From 'n' file, in MeV
    disp(['Pulse energy: ' num2str(pulse_energy_j) ' J = ' num2str(pulse_energy, '%10.3e') ' MeV']);

    nsteps = size(filenames,2);

    angles_cumulative = zeros(4, n_ebins+1, nsteps);
    angles_differential = zeros(4, n_ebins+1, nsteps);
    times = zeros(1, nsteps);

    n = 0;

    max_ene = zeros(4);

    px_old = [];
    py_old = [];
    pz_old = [];
    
    for f=filenames
        %TODO: Sort and partition by energy instead of traversing the whole
        %      array for every energy bin
        n = n + 1;

        l=GetDataSDF(f{1});

        electron_species = fieldnames(l.Grid.Particles.subset_elns);

        px = []; py = []; pz = []; w = [];

        for i=1:numel(electron_species)
            px = [px; l.Particles.Px.subset_elns.(electron_species{i}).data];
            py = [py; l.Particles.Py.subset_elns.(electron_species{i}).data];
            pz = [pz; l.Particles.Pz.subset_elns.(electron_species{i}).data];
            w = [w; l.Particles.Weight.subset_elns.(electron_species{i}).data];
        end

        ene=m_e*3e8^2*(1/(m_e*3e8)*sqrt((m_e*3e8)^2+px.^2+py.^2+pz.^2)-1) /el_chg/1e6;  % in MeV

        alpha=atan2(py,px)/pi*180;
        neg_ang = find(alpha<1);
        alpha(neg_ang) = alpha(neg_ang)+360;
        
        times(1,n) = l.time;

        for quadrant = display_quads
            idx_all = find(alpha >= (quadrant-1)*90 & alpha < quadrant*90);
            if any(idx_all)

                max_ene_now = max(ene(idx_all));
                disp(['t = ' num2str(l.time) ', e^- quad ' num2str(quadrant) ': Emax = ' num2str(max_ene_now)]);
                max_ene(quadrant) = max(max_ene_now, max_ene(quadrant));

                if cumulative == 1
                    for i = 1:size(all_e,2)-1   
                        idx = intersect(find(ene>all_e(i) & ene<all_e(i+1)), idx_all);
                        angles_cumulative(quadrant, i, n) = mean(alpha(idx));
                    end
                end

                if differential == 1
                    %TODO: This will wrongly remove photons with exact
                    %      same momentum...
                    pxdiff = ~ismember(px, px_old);
                    pydiff = ~ismember(py, py_old);
                    pzdiff = ~ismember(pz, pz_old);
                    iarr_dif = pxdiff | pydiff | pzdiff;
                    for i = 1:size(all_e,2)-1   
                        idx = intersect(find(ene>all_e(i) & ene<all_e(i+1) & iarr_dif), idx_all);
                        angles_differential(quadrant, i, n) = mean(alpha(idx));
                    end
                end
            else
                disp(['t = ' num2str(l.time) ', no electrons in quad ' num2str(quadrant)]);
            end
        end


        px_old = px;
        py_old = py;
        pz_old = pz;

    end
    
    draw_graph();
    
    export_variables();

    function draw_graph()
        for quad = display_quads
            
            ncurves = size(angles_cumulative, 2);
            
            if cumulative == 1
                figure;
                hold on;
                for i = 1:ncurves
                    mx = all_e(i+1);
                    if isinf(mx)
                        mx = max_ene(quad);
                    end
                    rat = i/ncurves;
                    plot(times, squeeze(angles_cumulative(quad,i,:)), ...
                        'LineWidth', 2, 'Color', [rat   0.25*(quad-1)  1-0.5*rat], 'DisplayName', ...
                        [num2str(round(all_e(i),round_n)) ' - ' num2str(round(mx,round_n)) ' MeV']);
                end
                title(['e^-, ' simtitle ', quad ' num2str(quad)]);
                legend('show');
                xlabel('Time [s]');
                ylabel('Mean angle [deg]');
                xlim([min(times) max(times)]);
                ylim(90*[quad-1 quad]);
                hold off;
                drawnow;
            end

            if differential == 1
                figure;
                hold on;
                for i = 1:ncurves
                    mx = all_e(i+1);
                    if isinf(mx)
                        mx = max_ene(quad);
                    end
                    rat = i/ncurves;
                    plot(times, squeeze(angles_differential(quad,i,:)), ...
                        'LineWidth', 2, 'Color', [rat   0.25*(quad-1)  1-0.5*rat], 'DisplayName', ...
                        [num2str(round(all_e(i),round_n)) ' - ' num2str(round(mx,round_n)) ' MeV']);
                end
                title(['e^- differential, ' simtitle ', quad ' num2str(quad)]);
                legend('show');
                xlabel('Time [s]');
                ylabel('Mean angle [deg]');
                xlim([min(times) max(times)]);
                ylim(90*[quad-1 quad]);
                hold off;
                drawnow;
            end
        end
    end

    function export_variables
        if export_results
            assignin('base', ['times_' num2str(simid)], times);
            if cumulative == 1
                assignin('base', ['angles_cumulative_' num2str(simid)], angles_cumulative);
            end
            if differential == 1
                assignin('base', ['angles_differential_' num2str(simid)], angles_differential);
            end
        end
    end

end

