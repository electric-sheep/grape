function deck = load_input_deck(fulldir)
    fullname = fullfile(fulldir, 'input.deck');
    deck = fileread(fullname);
end
