% Script that finds the maximum electron energy in several files

max_ene = 0;
ts_maxima = [];

for timestep=1:300

    h5elnfile = sprintf('peh%04i.h5', timestep);

    px=h5read(h5elnfile, '/elns_hi_electron_full/Px');
    py=h5read(h5elnfile, '/elns_hi_electron_full/Px');
    pz=h5read(h5elnfile, '/elns_hi_electron_full/Px');

    ene = m_e*c^2 * (1/(m_e*c) * sqrt((m_e*c)^2 + px.^2 + py.^2 + pz.^2) - 1) / el_chg / 1e6;

    max_here = max(ene);
    ts_maxima(timestep) = max_here;
    
    max_ene = max(max_ene, max_here);
end

disp('max ene = ' max_ene]);