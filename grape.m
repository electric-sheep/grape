function grape

% Requires GUI Layout Toolbox https://www.mathworks.com/matlabcentral/fileexchange/47982-gui-layout-toolbox

settings = struct( ...
    'topdir', '/media/simulations', ...
    'outdir', '/home/strazce/research/gammas' ...
);

db = init_database();
dirs = process_directories(db);

readable_params = {
    'none'                          'simulation'
    'foil_thickness_um'             'foil thickness [\mum]'
    'las_intensity_focus_w_cm2'     'laser intensity in focus [W/cm^2]'
    'ramp_length_um'                'density ramp length [\mum]'
    'pulse_length'                  'pulse duration [fs]'
    'n_el_over_nc'                  'n_{electrons} / n_{critical}'
    'bs_xsection_type'              'Bremsstrahlung cross-section type'
    'spot_size'                     'spot size (sigma)'
    'xfocus'                        'focus position [m]'
    'preplasma_length_um'           'pre-plasma characteristic length [\mum]'
};

vt = viz_types();

steps = struct( 'available', {[]}, ...
                'selected', {[]}, ...
                'last_only', 0, ...
                'hi_res', 0 );

gui = create_interface(vt, dirs);

    function d = process_directories(db)
        valid_indexes = find(~cellfun(@isempty,db));
        valid_cells = db(valid_indexes);
        directories = cellfun(@(x) x{1}, valid_cells, 'UniformOutput', 0);
        d = struct( ...
            'ids', {valid_indexes}, ...
            'names', {directories}, ...
            'selected', [] ...
        );
    end

    function viz_type = viz_types() 
        viz_list = {
            'Angular distribution'          'butterfly'                 'pf'
            'Gamma conversion efficiency'   'eband_efficiency'          'pf'
            'Time vs. energy'               'time_vs_energy'            'pf'
            'Time vs. angle spread'         'time_vs_angle_spread'      'pf'
            'Gamma spectrum'                'spectrum'                  'pf'
            'e- time vs. angle spread'      'time_vs_angle_spread_e'    'pe'
            'e- butterfly graph'            'butterfly_e'               'pe'
            'e- phasespace'                 'phasespace_e'              'pe'
            'e- spectrum'                   'spectrum_e'                'pe'
            'Energy balance'                'energy_balance'            'pe'
            'Ion conversion efficiency'     'ion_efficiency'            'pa'
            'Time vs. energy at detector'   'time_vs_energy_detector'   'pf'
            'Eta histogram'                 'eta'                       'pe'
        };
        viz_settings = {    % This has to be in sync with update_viz_settings()
            struct(... % butterfly
                'bs', 1, 'cs', 1, 'tags', 1, ...
                'butterfly', 1, ...
                'abin_size', 3, 'phot_ratio', 1, ...
                'emin', 1, 'ebin_size', 1, 'n_ebins', 4, ...
                'percentiles', 0, ...
                'differential', 0, 'cumulative', 1, ...
                'boundary_y', 1, ...
                'display', 1, 'save', 0 ...
                ), ...
            struct(... % eband_efficiency
                'bs', 1, 'cs', 1, 'tags', 1, ...
                'phot_ratio', 1, ...
                'emin', 1, 'emax', 5, ...
                'plot_against', 1, ...
                'angle_restriction', 90, ...
                'boundary_y', 1, 'bar_graph', 0 ...
                ) ...
            struct(... % time_vs_energy
                'export_results', 1, ...
                'bs', 1, 'cs', 1, 'tags', 1, ...
                'phot_ratio', 1, 'differential', 1, 'cumulative', 1, ...
                'emin', 1, 'ebin_size', 1, 'n_ebins', 4, ...
                'round', 0, 'angle_restriction', 90, ...
                'forward', 1, 'backward', 1, 'both', 1, ...
                'boundary_y', 1 ...
                ) ...
            struct(... % time_vs_angle_spread
                'export_results', 1, ...
                'bs', 1, 'cs', 1, 'tags', 1, ...
                'phot_ratio', 1, 'differential', 1, 'cumulative', 1, ...
                'emin', 1, 'ebin_size', 1, 'n_ebins', 4, ...
                'round', 0, 'quads', [1 0 0 0], ...
                'combined_quads', 1, 'separated_quads', 0, ...
                'boundary_y', 1 ...
                ) ... 
            struct(... % spectrum
                'export_results', 1, ...
                'bs', 1, 'cs', 1, 'tags', 1, ...
                'emin', 0.1, 'emax', inf, ...
                'phot_ratio', 1, 'smoothing', 3, ...
                'angle_restriction', 90, 'boundary_y', 1 ...
                ) ...
            struct(... % time_vs_angle_spread_e
                'export_results', 1, ...
                'eln_ratio', 0.1, 'differential', 1, 'cumulative', 1, ...
                'emin', 1, 'ebin_size', 1, 'n_ebins', 4, ...
                'round', 0, 'quads', [1 0 0 0], ...
                'boundary_y', 1 ...
                ) ... 
            struct(... % butterfly_e
                'abin_size', 3, 'eln_ratio', 0.1, ...
                'emin', 1, 'ebin_size', 2, 'n_ebins', 6, ...
                'boundary_y', 1 ...
                ), ...                
            struct(... % phasespace_e
                'resolution', 1000, 'eln_ratio', 0.1, ...
                'xmin', -5, 'xmax', 25, ...
                'ymin', -20, 'ymax', 20, ...
                'zmin', -20, 'zmax', 20, ...
                'pmin', -150, 'pmax', 150, ...
                'emin', 0, ...
                'x_x', 1, 'x_y', 1, ...
                'x_px', 0, 'x_py', 0, 'x_pz', 0, ...
                'y_x', 0, 'y_y', 0, ...
                'y_px', 1, 'y_py', 1, 'y_pz', 0, ...
                'display', 1, 'save', 0, ...
                'boundary_y', 1 ...
                ), ...
            struct(... % spectrum_e
                'export_results', 1, 'energy_eta', 1, ...
                'integrated', 0, 'eln_ratio', 0.1, ...
                'emin', 1, 'emax', inf, ...
                'smoothing', 3, ...
                'angle_restriction', 90, 'boundary_y', 1, ...
                'xmin', -Inf, 'xmax', Inf ...
                ) ...
            struct(... % energy_balance
                'export_results', 1, ...
                'eln_ratio', 0.1, ...
                'ion_ratio', 0.1, ...
                'phot_ratio', 1, ...
                'boundary_y', 1 ...
                ) ...
            struct(... % ion_efficiency
                'ion_ratio', 0.1, ...
                'emin', 0, 'emax', Inf, ...
                'plot_against', 1, ...
                'angle_restriction', 90, ...
                'boundary_y', 1, 'bar_graph', 0, ...
                'xmin', -Inf, 'xmax', Inf, ...
                'plus_thickness', 0 ...
                ) ...
            struct(... % time_vs_energy_detector
                'export_results', 1, ...
                'bs', 1, 'cs', 1, 'tags', 1, ...
                'phot_ratio', 1, ...
                'emin', 1, 'ebin_size', 1, 'n_ebins', 4, ...
                'round', 0, 'angle_restriction', 90, ...
                'forward', 1, 'backward', 1, 'both', 1, ...
                'detector_position_um', 100, ...
                'boundary_y', 1 ...
                ) ...
            struct(... % eta
                'export_results', 1, 'display', 1, ...
                'emax', 0.1, ...
                'integrated', 0, 'eln_ratio', 1.0, ...
                'smoothing', 0.0003, ...
                'angle_restriction', 90, 'boundary_y', 1, ...
                'xmin', -Inf, 'xmax', Inf ...
                ) ...
        };
    
        viz_type = struct( ...
            'names', {viz_list(:,1)'}, ...
            'functions', {viz_list(:,2)'}, ...
            'needed_files', {viz_list(:,3)'}, ...
            'settings', {viz_settings}, ...
            'selected', 1 ...
            );
    end

    function gui = create_interface(vt, dirs)
        gui = struct();
        gui.vset = struct();
        gui.window = figure( 'Name', 'GRAPE: Gamma Ray Analysis Platform for EPOCH', ...
            'NumberTitle', 'off', ...
            'MenuBar', 'none', ...
            'Toolbar', 'none', ...
            'HandleVisibility', 'off', ...
            'Position', [800 300 1200 1000]);
        
        main_layout = uix.VBoxFlex( 'Parent', gui.window, 'Spacing', 3);
        
        wrapper_layout = uix.HBox( 'Parent', main_layout );
        
        gui.viz_type_layout = uix.VBox('Parent', wrapper_layout );
        gui.viz_type_panel = uiextras.BoxPanel( ...
            'Parent', gui.viz_type_layout, ...
            'Title', 'Visualization type:' );
        
        gui.viz_type = uicontrol('Style', 'list', ...
            'Parent', gui.viz_type_layout, ...
            'String', vt.names(:), ...
            'Callback', @on_viz_type_selection );
        
        gui.viz_settings = uix.VBox( 'Parent', gui.viz_type_layout);

        set(gui.viz_type_layout, 'Heights', [28 160 -1]);
        
        gui.simulation_layout = uix.VBox('Parent', wrapper_layout );
        gui.source_panel = uiextras.BoxPanel( ...
            'Parent', gui.simulation_layout, ...
            'Title', 'Simulations:' );
        gui.dir_list = uicontrol( 'Style', 'list', ...
            'Parent', gui.simulation_layout, ...
            'String', dirs.names, ...
            'Max', size(dirs.names, 1), ...
            'Callback', @on_directory_selection );
        
        set(gui.simulation_layout, 'Heights', [28 -1]);
        
        timestep_layout = uix.VBox( 'Parent', wrapper_layout );
        gui.timestep_panel = uiextras.BoxPanel( ...
            'Parent', timestep_layout, ...
            'Title', 'Time steps:' );
        
        gui.last_step = uicontrol( 'Style', 'checkbox',...
            'Parent', timestep_layout, ...
            'String', 'use last available step', ...
            'Callback', @on_last_step_box);
        gui.hi_res = uicontrol( 'Style', 'checkbox',...
            'Parent', timestep_layout, ...
            'String', 'hi-res outputs', ...
            'Callback', @on_hi_res_box);
        gui.timesteps = uicontrol( 'Style', 'list', ...
            'Parent', timestep_layout, ...
            'Callback', @on_timesteps_selection );
        
        set(timestep_layout, 'Heights', [28 28 28 -1]);
        
        controls_layout = uix.HBox( 'Parent', main_layout );
        gui.draw_button = uicontrol( 'Style', 'PushButton',...
            'Parent', controls_layout, ...
            'String', 'Draw!', ...
            'Callback', @on_draw_button);
        
        set(main_layout, 'Heights', [-1 28]);
    end

    function update_interface()
        
        if isempty(steps.available)
            set(gui.timesteps, 'String', 'no matching files');
        else
            filenames = strcat(steps.available, '.sdf');
            set(gui.timesteps, 'String', filenames);
            len = max(2, length(filenames));    % Listbox must have at least two values (https://www.mathworks.com/matlabcentral/newsreader/view_thread/254765)
            set(gui.timesteps, 'Max', len);
            if steps.last_only == 1
                set(gui.timesteps, 'Value', size(steps.available,1));
            else
                set(gui.timesteps, 'Value', steps.selected);
            end
        end
        
        update_viz_settings();
    end

    function on_viz_type_selection(src, ~)
        vt.selected = get(src, 'Value');
        reset_timesteps();
        update_interface();
    end

    function on_directory_selection(src, ~)
        dirs.selected = get(src, 'Value');
        reset_timesteps();
        update_interface();
    end

    function on_timesteps_selection(src, ~)
        steps.selected = get(src, 'Value');
        update_interface();
    end

    function on_last_step_box(src, ~)
        steps.last_only = get(src, 'Value');
        update_interface();
    end

    function on_hi_res_box(src, ~)
        steps.hi_res = get(src, 'Value');
        reset_timesteps();
        update_interface();
    end

    function update_viz_settings()
        if ishandle(gui.viz_settings)
            delete(gui.viz_settings);
        end
        gui.viz_settings = uix.VBox( 'Parent', gui.viz_type_layout);
        vset = gui.vset;
        st = vt.settings{vt.selected};
        hs = [];
        switch vt.selected
            case 1 % butterfly
                add_checkbox('display', 'Display all figures', vset, st); hs = [hs 20];
                add_checkbox('save', 'Save generated figures', vset, st); hs = [hs 20];
                add_checkbox('bs', 'Bremsstrahlung', vset, st); hs = [hs 20];
                add_checkbox('cs', 'Inverse Compton Scattering', vset, st); hs = [hs 20];
                add_checkbox('tags', 'Use tags', vset, st); hs = [hs 20];
                add_checkbox('butterfly', 'Butterfly graph', vset, st); hs = [hs 20];
                add_checkbox('percentiles', 'Percentiles above min instead of emax', vset, st); hs = [hs 20];
                add_checkbox('differential', 'Plot differential', vset, st); hs = [hs 20];
                add_checkbox('cumulative', 'Plot cumulative', vset, st); hs = [hs 20];
                add_numval('abin_size', 'Angular bin size: ', vset, st); hs = [hs 20];
                add_numval('emin', 'Minimum photon energy: ', vset, st); hs = [hs 20];
                add_numval('ebin_size', 'Energy bin size: ', vset, st); hs = [hs 20];
                add_numval('n_ebins', 'Number of energy bins: ', vset, st); hs = [hs 20];
                add_numval('boundary_y', 'Omit y boundary [um]: ', vset, st); hs = [hs 20];
                
            case 2 % eband_efficiency
                add_checkbox('bs', 'Bremsstrahlung', vset, st); hs = [hs 20];
                add_checkbox('cs', 'Inverse Compton Scattering', vset, st); hs = [hs 20];
                add_checkbox('tags', 'Use tags', vset, st); hs = [hs 20];
                add_numval('emin', 'Minimum photon energy: ', vset, st); hs = [hs 20];
                add_numval('emax', 'Maximum photon energy: ', vset, st); hs = [hs 20];
                add_numval('boundary_y', 'Omit y boundary [um]: ', vset, st); hs = [hs 20];
                add_numval('angle_restriction', 'Angle restriction (fwd) [deg]: ', vset, st); hs = [hs 20];
                add_checkbox('bar_graph', 'Bar graph', vset, st); hs = [hs 20];
                
                layout_plot_against = uix.VBox('Parent', gui.viz_settings);
                uicontrol('Parent', layout_plot_against, 'Style', 'Text', 'String', 'Plot against: ');                
                vset.plot_against = uicontrol('Parent', layout_plot_against, 'Style', 'List', 'String', readable_params(:,1), ...
                    'Value', st.plot_against, 'Callback', {@on_vset_plot_against, vt.selected});
                set(layout_plot_against, 'Heights', [20 -1]);
                hs = [hs -1];

            case 3 % time_vs_energy
                add_checkbox('export_results', 'Export results', vset, st); hs = [hs 20];
                add_checkbox('bs', 'Bremsstrahlung', vset, st); hs = [hs 20];
                add_checkbox('cs', 'Inverse Compton Scattering', vset, st); hs = [hs 20];
                add_checkbox('tags', 'Use tags', vset, st); hs = [hs 20];
                add_checkbox('differential', 'Plot differential', vset, st); hs = [hs 20];
                add_checkbox('cumulative', 'Plot cumulative', vset, st); hs = [hs 20];
                add_checkbox('forward', 'Forward only', vset, st); hs = [hs 20];
                add_checkbox('backward', 'Backward only', vset, st); hs = [hs 20];
                add_checkbox('both', 'Both directions', vset, st); hs = [hs 20];
                add_numval('emin', 'Minimum photon energy: ', vset, st); hs = [hs 20];
                add_numval('ebin_size', 'Energy bin size: ', vset, st); hs = [hs 20];
                add_numval('n_ebins', 'Number of energy bins: ', vset, st); hs = [hs 20];
                add_numval('angle_restriction', 'Angle restriction [deg]: ', vset, st); hs = [hs 20];
                add_numval('boundary_y', 'Omit y boundary [um]: ', vset, st); hs = [hs 20];
                add_numval('round', 'Rounding to decimal: ', vset, st); hs = [hs 20];
                
            case 4 % time_vs_angle_spread
                add_checkbox('export_results', 'Export results', vset, st); hs = [hs 20];
                add_checkbox('bs', 'Bremsstrahlung', vset, st); hs = [hs 20];
                add_checkbox('cs', 'Inverse Compton Scattering', vset, st); hs = [hs 20];
                add_checkbox('tags', 'Use tags', vset, st); hs = [hs 20];
                add_checkbox('differential', 'Plot differential', vset, st); hs = [hs 20];
                add_checkbox('cumulative', 'Plot cumulative', vset, st); hs = [hs 20];
                add_quads(vset, st); hs = [hs 20 20 20 20];
                add_checkbox('separated_quads', 'Separated quads', vset, st); hs = [hs 20];
                add_checkbox('combined_quads', 'Combined quads', vset, st); hs = [hs 20];
                add_numval('emin', 'Minimum photon energy: ', vset, st); hs = [hs 20];
                add_numval('ebin_size', 'Energy bin size: ', vset, st); hs = [hs 20];
                add_numval('n_ebins', 'Number of energy bins: ', vset, st); hs = [hs 20];
                add_numval('boundary_y', 'Omit y boundary [um]: ', vset, st); hs = [hs 20];
                add_numval('round', 'Rounding to decimal: ', vset, st); hs = [hs 20];

            case 5 % spectrum
                add_checkbox('export_results', 'Export results', vset, st); hs = [hs 20];
                add_checkbox('bs', 'Bremsstrahlung', vset, st); hs = [hs 20];
                add_checkbox('cs', 'Inverse Compton Scattering', vset, st); hs = [hs 20];
                add_checkbox('tags', 'Use tags', vset, st); hs = [hs 20];
                add_numval('smoothing', 'Smoothing: ', vset, st); hs = [hs 20];
                add_numval('emin', 'Minimum photon energy: ', vset, st); hs = [hs 20];
                add_numval('emax', 'Maximum photon energy: ', vset, st); hs = [hs 20];                
                add_numval('angle_restriction', 'Angle restriction [deg]: ', vset, st); hs = [hs 20];
                add_numval('boundary_y', 'Omit y boundary [um]: ', vset, st); hs = [hs 20];

            case 6 % time_vs_angle_spread_e
                add_checkbox('export_results', 'Export results', vset, st); hs = [hs 20];
                add_checkbox('differential', 'Plot differential', vset, st); hs = [hs 20];
                add_checkbox('cumulative', 'Plot cumulative', vset, st); hs = [hs 20];
                add_quads(vset, st); hs = [hs 20 20 20 20];
                add_numval('eln_ratio', 'Electron output ratio: ', vset, st); hs = [hs 20];
                add_numval('emin', 'Minimum electron energy: ', vset, st); hs = [hs 20];
                add_numval('ebin_size', 'Energy bin size: ', vset, st); hs = [hs 20];
                add_numval('n_ebins', 'Number of energy bins: ', vset, st); hs = [hs 20];
                add_numval('boundary_y', 'Omit y boundary [um]: ', vset, st); hs = [hs 20];
                add_numval('round', 'Rounding to decimal: ', vset, st); hs = [hs 20];
                
            case 7 % butterfly_e
                add_numval('eln_ratio', 'Electron output ratio: ', vset, st); hs = [hs 20];
                add_numval('abin_size', 'Angular bin size: ', vset, st); hs = [hs 20];
                add_numval('emin', 'Minimum electron energy: ', vset, st); hs = [hs 20];
                add_numval('ebin_size', 'Energy bin size: ', vset, st); hs = [hs 20];
                add_numval('n_ebins', 'Number of energy bins: ', vset, st); hs = [hs 20];
                add_numval('boundary_y', 'Omit y boundary [um]: ', vset, st); hs = [hs 20];
                
            case 8 % phasespace_e
                add_checkbox('display', 'Display all figures', vset, st); hs = [hs 20];
                add_checkbox('save', 'Save generated figures', vset, st); hs = [hs 20];
                add_numval('eln_ratio', 'Electron output ratio: ', vset, st); hs = [hs 20];
                add_numval('resolution', 'Graph resolution: ', vset, st); hs = [hs 20];
                add_numval('boundary_y', 'Omit y boundary [um]: ', vset, st); hs = [hs 20];
                add_numval('xmin', 'xmin [um]: ', vset, st); hs = [hs 20];
                add_numval('xmax', 'xmax [um]: ', vset, st); hs = [hs 20];
                add_numval('ymin', 'ymin [um]: ', vset, st); hs = [hs 20];
                add_numval('ymax', 'ymax [um]: ', vset, st); hs = [hs 20];
                add_numval('zmin', 'zmin [um]: ', vset, st); hs = [hs 20];
                add_numval('zmax', 'zmax [um]: ', vset, st); hs = [hs 20];
                add_numval('pmin', 'pmin [um]: ', vset, st); hs = [hs 20];
                add_numval('pmax', 'pmax [um]: ', vset, st); hs = [hs 20];
                add_numval('emin', 'Minimum electron energy: ', vset, st); hs = [hs 20];

                layout_axes = uix.HBox('Parent', gui.viz_settings);
                
                layout_xax =  uix.VBox('Parent', layout_axes);
                uicontrol('Parent', layout_xax, 'Style', 'Text', 'String', 'x axis');
                add_checkbox('x_x', 'x', vset, st, layout_xax);
                add_checkbox('x_y', 'y', vset, st, layout_xax);
                add_checkbox('x_px', 'px', vset, st, layout_xax);
                add_checkbox('x_py', 'py', vset, st, layout_xax);
                add_checkbox('x_pz', 'pz', vset, st, layout_xax);

                layout_yax =  uix.VBox('Parent', layout_axes);
                uicontrol('Parent', layout_yax, 'Style', 'Text', 'String', 'y axis');
                add_checkbox('y_x', 'x', vset, st, layout_yax);
                add_checkbox('y_y', 'y', vset, st, layout_yax);
                add_checkbox('y_px', 'px', vset, st, layout_yax);
                add_checkbox('y_py', 'py', vset, st, layout_yax);
                add_checkbox('y_pz', 'pz', vset, st, layout_yax);
                
                hs = [hs 80];
                
            case 9 % spectrum_e
                add_checkbox('export_results', 'Export results', vset, st); hs = [hs 20];
                add_numval('smoothing', 'Smoothing: ', vset, st); hs = [hs 20];
                add_numval('eln_ratio', 'Electron output ratio: ', vset, st); hs = [hs 20];
                add_numval('emin', 'Minimum electron energy: ', vset, st); hs = [hs 20];
                add_numval('emax', 'Maximum electron energy: ', vset, st); hs = [hs 20];                
                add_numval('angle_restriction', 'Angle restriction [deg]: ', vset, st); hs = [hs 20];
                add_numval('boundary_y', 'Omit y boundary [um]: ', vset, st); hs = [hs 20];
                add_numval('xmin', 'Minimum x [um]: ', vset, st); hs = [hs 20];
                add_numval('xmax', 'Maximum x [um]: ', vset, st); hs = [hs 20];
                
            case 10 % energy_balance
                add_checkbox('export_results', 'Export results', vset, st); hs = [hs 20];
                add_numval('eln_ratio', 'Electron output ratio: ', vset, st); hs = [hs 20];
                add_numval('ion_ratio', 'Ion output ratio: ', vset, st); hs = [hs 20];
                add_numval('phot_ratio', 'Photon output ratio: ', vset, st); hs = [hs 20];
                add_numval('boundary_y', 'Omit y boundary [um]: ', vset, st); hs = [hs 20];

            case 11 % ion_efficiency
                add_numval('ion_ratio', 'Ion output ratio: ', vset, st); hs = [hs 20];
                add_numval('emin', 'Minimum ion energy: ', vset, st); hs = [hs 20];
                add_numval('emax', 'Maximum ion energy: ', vset, st); hs = [hs 20];
                add_numval('boundary_y', 'Omit y boundary [um]: ', vset, st); hs = [hs 20];
                add_numval('xmin', 'Minimum x [um]: ', vset, st); hs = [hs 20];
                add_numval('xmax', 'Maximum x [um]: ', vset, st); hs = [hs 20];
                add_checkbox('plus_thickness', 'Add foil thickness to xmin', vset, st); hs = [hs 20];
                add_numval('angle_restriction', 'Angle restriction (fwd) [deg]: ', vset, st); hs = [hs 20];
                add_checkbox('bar_graph', 'Bar graph', vset, st); hs = [hs 20];
                
                layout_plot_against = uix.VBox('Parent', gui.viz_settings);
                uicontrol('Parent', layout_plot_against, 'Style', 'Text', 'String', 'Plot against: ');                
                vset.plot_against = uicontrol('Parent', layout_plot_against, 'Style', 'List', 'String', readable_params(:,1), ...
                    'Value', st.plot_against, 'Callback', {@on_vset_plot_against, vt.selected});
                set(layout_plot_against, 'Heights', [20 -1]);
                hs = [hs -1];
               
                
            case 12 % time_vs_energy_detector
                add_checkbox('export_results', 'Export results', vset, st); hs = [hs 20];
                add_checkbox('bs', 'Bremsstrahlung', vset, st); hs = [hs 20];
                add_checkbox('cs', 'Inverse Compton Scattering', vset, st); hs = [hs 20];
                add_checkbox('tags', 'Use tags', vset, st); hs = [hs 20];
                add_checkbox('forward', 'Forward only', vset, st); hs = [hs 20];
                add_checkbox('backward', 'Backward only', vset, st); hs = [hs 20];
                add_checkbox('both', 'Both directions', vset, st); hs = [hs 20];
                add_numval('emin', 'Minimum photon energy: ', vset, st); hs = [hs 20];
                add_numval('ebin_size', 'Energy bin size: ', vset, st); hs = [hs 20];
                add_numval('n_ebins', 'Number of energy bins: ', vset, st); hs = [hs 20];
                add_numval('detector_position_um', 'Detector position [um]: ', vset, st); hs = [hs 20];
                add_numval('angle_restriction', 'Angle restriction [deg]: ', vset, st); hs = [hs 20];
                add_numval('boundary_y', 'Omit y boundary [um]: ', vset, st); hs = [hs 20];
                add_numval('round', 'Rounding to decimal: ', vset, st); hs = [hs 20];
                
                
            case 13 % eta
                add_checkbox('display', 'Display all figures', vset, st); hs = [hs 20];
                add_checkbox('export_results', 'Export results', vset, st); hs = [hs 20];
                add_checkbox('integrated', 'Time integrated', vset, st); hs = [hs 20];
                add_numval('smoothing', 'Smoothing: ', vset, st); hs = [hs 20];
                add_numval('eln_ratio', 'Electron output ratio: ', vset, st); hs = [hs 20];
                add_numval('emax', 'Maximum eta value: ', vset, st); hs = [hs 20];
                add_numval('angle_restriction', 'Angle restriction [deg]: ', vset, st); hs = [hs 20];
                add_numval('boundary_y', 'Omit y boundary [um]: ', vset, st); hs = [hs 20];
                add_numval('xmin', 'Minimum x [um]: ', vset, st); hs = [hs 20];
                add_numval('xmax', 'Maximum x [um]: ', vset, st); hs = [hs 20];
        end
        set(gui.viz_settings, 'Heights', hs);
    end

    function add_checkbox(varname, title, vset, st, parent)
        if nargin == 4
            parent = gui.viz_settings;
        end
        vset.(varname) = uicontrol('Parent', parent, 'Style', 'Checkbox', 'String', title, ...
                    'Value', st.(varname), 'Callback', {@on_vset_checkbox, vt.selected, varname});
    end

    function add_numval(varname, title, vset, st)
            layout_loc = uix.HBox('Parent', gui.viz_settings);
            uicontrol('Parent', layout_loc, 'Style', 'Text', 'String', title);
            vset.(varname) = uicontrol('Parent', layout_loc, 'Style', 'Edit', 'String', st.(varname), ...
                    'Value', st.(varname), 'Callback', {@on_vset_numval, vt.selected, varname});
    end

    function add_quads(vset, st)
        vset.quad1 = uicontrol('Parent', gui.viz_settings,'Style', 'Checkbox', 'String', '1st quadrant', ...
            'Value', st.quads(1), 'Callback', {@on_vset_quads, vt.selected, 1});
        vset.quad2 = uicontrol('Parent', gui.viz_settings,'Style', 'Checkbox', 'String', '2nd quadrant', ...
            'Value', st.quads(2), 'Callback', {@on_vset_quads, vt.selected, 2});
        vset.quad3 = uicontrol('Parent', gui.viz_settings,'Style', 'Checkbox', 'String', '3rd quadrant', ...
            'Value', st.quads(3), 'Callback', {@on_vset_quads, vt.selected, 3});
        vset.quad4 = uicontrol('Parent', gui.viz_settings,'Style', 'Checkbox', 'String', '4th quadrant', ...
            'Value', st.quads(4), 'Callback', {@on_vset_quads, vt.selected, 4});
    end

    function on_vset_checkbox(src, ~, vid, varname)
        vt.settings{vid}.(varname) = get(src, 'Value');
        update_interface();
    end

    function on_vset_numval(src, ~, vid, varname)
        vt.settings{vid}.(varname) = str2double(get(src, 'String'));
        update_interface();
    end

    function on_vset_quads(src, ~, vid, qnum)
        vt.settings{vid}.quads(qnum) = get(src, 'Value');
        update_interface();
    end

    function on_vset_plot_against(src, ~, vid)
        vt.settings{vid}.plot_against = get(src, 'Value');
        update_interface();
    end

    function fns = list_files(dirname, filetype)
        fpath = fullfile(settings.topdir, dirname, filetype);
        fmask = strcat(fpath, '*.sdf');         % Mask only allows * wildcard. 'pf' filetype would also match 'pfh0001.sdf'
        files = dir(fmask{1});
        filenames = struct2cell(files);
        filenames = filenames(1,:);

        fns={};
        for fn = filenames
            r = regexp(fn{1}, strcat(filetype, '[0-9]+\.sdf'), 'once');
            if ~isempty( r{1} )   % Check if the filetype prefix matches exactly
                nm = strrep(fn{1}, '.sdf', '');
                rtime = regexp(nm, '\d+', 'match');
                time  = str2num(rtime{1});
                rtype = regexp(nm, '^[a-z]+', 'match');
                type  = rtype{1};
                fns = [fns; {nm} {type} {time}];
            end
        end
    end

    function reset_timesteps
        if isempty(dirs.selected)
            return
        end
        set(gui.timesteps, 'String', '');
        filetype = vt.needed_files(vt.selected);
        if steps.hi_res == 1
            filetype = strcat(filetype, 'h');
        end
        files = list_files(dirs.names(dirs.selected(1)), filetype);
        if isempty(files)
            common_steps = [];
        else
            common_steps = files(:,1);
            for d = dirs.selected
                files = list_files(dirs.names(d), filetype);
                if isempty(files)
                    common_steps = [];
                else                
                    common_steps = intersect(common_steps, files(:,1));
                end
            end
        end
        steps.available = common_steps;
    end

    function on_draw_button(~, ~)
        func = vt.functions(vt.selected);
        update_interface();
        disp(datestr(now));
        fcall_timer = tic;
        evalin('base', func{1});
        disp(datestr(now));
        toc(fcall_timer);
    end

    function sel = get_selection(d, s)
        fname = strcat(steps.available(s), '.sdf');
        id = dirs.ids(d);
        sel = struct( ...
            'fulldir', fullfile(settings.topdir, dirs.names(d)), ...
            'fullname', fullfile(settings.topdir, dirs.names(d), fname), ...
            'simtitle', db{id}{2}, ...
            'pulse_energy', db{id}{3}, ...
            'dirname', dirs.names(d) ...
        );
    end

    function butterfly()
        graph_settings = vt.settings{vt.selected};
        fig = 0;
        if (~graph_settings.display) && graph_settings.save
            fig = figure;
            drawnow;
        end
        for d = sort(dirs.selected)
            fullnames = {};
            simtitles = {};
            pulse_energies = [];            
            for s = sort(steps.selected)
                sel = get_selection(d, s);
                fullnames{end+1} = sel.fullname;
            end
            stats = graph_butterfly(fullnames, ...
                    sel.simtitle, ...
                    sel.pulse_energy, ...
                    graph_settings, ...
                    [settings.outdir '/butterfly/' sel.dirname], ...
                    fig);
        end
        
    end


    function butterfly_e()
        graph_settings = vt.settings{vt.selected};
        for d = sort(dirs.selected)
            for s = sort(steps.selected)
                sel = get_selection(d, s);
                stats = graph_butterfly_e(sel.fullname, ...
                    sel.simtitle, ...
                    sel.pulse_energy, ...
                    graph_settings);
                disp(['Max energy: ' num2str(stats.max_energy) ' MeV']);
            end
        end
    end

    function phasespace_e()
        graph_settings = vt.settings{vt.selected};
        fig = 0;
        if (~graph_settings.display) && graph_settings.save
            fig = figure;
            drawnow;
        end
        for d = sort(dirs.selected)
            for s = sort(steps.selected)
                sel = get_selection(d, s);
                stats = graph_phasespace_e(sel.fullname, ...
                    sel.simtitle, ...
                    sel.pulse_energy, ...
                    graph_settings, ...
                    [settings.outdir '/phasespace/' sel.dirname], ...
                    fig);
                disp(['Max energy: ' num2str(stats.max_energy) ' MeV']);
            end
        end
    end

    function eband_efficiency
        graph_settings = vt.settings{vt.selected};
        arg_name = readable_params(graph_settings.plot_against,1);
        for s = sort(steps.selected)
            disp('      ---      ---      ---      ');
            args = [];
            total_energy_tt = zeros(0,2);
            energy_in_range_tt = zeros(0,2);
            low_energy_tt = zeros(0,2);
            high_energy_tt = zeros(0,2);
            max_energy_tt = zeros(0,2);
            pulse_energy_tt = [];
            i = 0;
            for d = sort(dirs.selected)
                i = i + 1;
                sel = get_selection(d, s);
                graph_settings.by = get_y_size_from_deck(sel.fulldir);
                if strcmp(arg_name{1}, 'none')
                    args(i) = i;
                else
                    deck = load_input_deck(sel.fulldir);
                    args(i) = get_input_deck_param(deck, arg_name);                    
                end
                disp(['Argument ' arg_name{1} ' = ' num2str(args(i))]);
                absorption = get_eband_efficiency(sel.fullname, ...
                    sel.simtitle, ...
                    sel.pulse_energy, ...
                    graph_settings);
                total_energy_tt(i,:) = absorption.total_energy;
                energy_in_range_tt(i,:) = absorption.energy_in_range;
                low_energy_tt(i,:) = absorption.low_energy;
                max_energy_tt(i,:) = absorption.max_energy;
                high_energy_tt(i,:) = absorption.high_energy;
                pulse_energy_tt(i) = absorption.pulse_energy;
            end
            
            [args, sort_index] = sort(args);
          
            for try_tag = 1:2
                if try_tag == 1
                    if graph_settings.cs == 0
                        continue
                    end
                    rad_type = 'CS';
                elseif try_tag == 2
                    if graph_settings.bs == 0
                        continue
                    end
                    rad_type = 'BS';
                end

                total_energy = total_energy_tt(:,try_tag);
                energy_in_range = energy_in_range_tt(:,try_tag);
                low_energy = low_energy_tt(:,try_tag);
                max_energy = max_energy_tt(:,try_tag);
                high_energy = high_energy_tt(:,try_tag);
                
                pulse_energy = pulse_energy_tt(sort_index);
                
                total_energy = total_energy(sort_index)';
                energy_in_range = energy_in_range(sort_index)';
                low_energy = low_energy(sort_index)';
                max_energy = max_energy(sort_index)';
                high_energy = high_energy(sort_index)';
                
                total_conversion = total_energy ./ pulse_energy;
                conversion_in_range = energy_in_range ./ pulse_energy;
                low_conversion = low_energy ./ pulse_energy;
                high_conversion = high_energy ./ pulse_energy;
                
                mx = max(max_energy);
                
                if graph_settings.bar_graph
                    figure;
                    hold on;
                    %cats = categorical( ...
                    %    {   [' < ' num2str(graph_settings.emin) ' MeV'], ...
                    %        [num2str(graph_settings.emin) ' - ' num2str(graph_settings.emax) ' MeV'], ...
                    %        [num2str(graph_settings.emin) ' - ' num2str(round(mx, 0)) ' MeV'] });
                    %h = bar(cats, [low_conversion; conversion_in_range; total_conversion]);
                    
                    cats  = {   [' < ' num2str(graph_settings.emin) ' MeV'], ...
                                [num2str(graph_settings.emin) ' - ' num2str(graph_settings.emax) ' MeV'], ...
                                [num2str(graph_settings.emax) ' - ' num2str(round(mx, 0)) ' MeV'], ...
                                [num2str(graph_settings.emin) ' - ' num2str(round(mx, 0)) ' MeV'] };
                    h = bar([low_conversion; conversion_in_range; high_conversion; total_conversion]);
                    set(gca, 'XTick', 1:4, 'XTickLabel', cats);
                    ylabel('Conversion efficiency');
                    title(rad_type);
                    lnames = cellstr(num2str(args(:)))';
                    legend(h, lnames);
                    legend('show');
                    hold off;
                    drawnow;
                else
                    figure;
                    hold on;
                    %TODO: Rounding in settings
                    if strcmp(arg_name{1}, 'none')
                        line_f = 'o';
                        line_l = 'o';
                    else
                        line_f = '-o';
                        line_l = ':o';
                    end
                    plot(args, total_conversion, line_f, 'LineWidth', 2, 'MarkerSize', 10, ...
                        'DisplayName', [num2str(graph_settings.emin) ' - ' num2str(round(mx, 0)) ' MeV']);
                    plot(args, conversion_in_range, line_f, 'LineWidth', 2, 'MarkerSize', 10, ...
                        'DisplayName', [num2str(graph_settings.emin) ' - ' num2str(graph_settings.emax) ' MeV']);
                    plot(args, high_conversion, line_f, 'LineWidth', 2, 'MarkerSize', 10, ...
                        'DisplayName', [num2str(graph_settings.emax) ' - ' num2str(round(mx, 0)) ' MeV']);
                    plot(args, low_conversion, line_l, 'LineWidth', 2, 'MarkerSize', 10, ...
                        'DisplayName', [' < ' num2str(graph_settings.emin) ' MeV']);
                    xlabel(readable_params(graph_settings.plot_against,2));
                    ylabel('Conversion efficiency');
                    title(rad_type);
                    legend('show');
                    hold off;
                    drawnow;
                end
            end
        end        
    end

    function time_vs_energy
        graph_settings = vt.settings{vt.selected};
        for d = sort(dirs.selected)
            fullnames = {};
            for s = sort(steps.selected)
                sel = get_selection(d, s);
                fullnames{end+1} = sel.fullname;
            end
            graph_time_vs_energy(fullnames, sel.simtitle, sel.pulse_energy, graph_settings, d);
        end  
    end

    function time_vs_energy_detector
        graph_settings = vt.settings{vt.selected};
        for d = sort(dirs.selected)
            fullnames = {};
            for s = sort(steps.selected)
                sel = get_selection(d, s);
                fullnames{end+1} = sel.fullname;
            end
            graph_time_vs_energy_detector(fullnames, sel.simtitle, sel.pulse_energy, graph_settings, d);
        end  
    end

    function time_vs_angle_spread
        graph_settings = vt.settings{vt.selected};
        for d = sort(dirs.selected)
            fullnames = {};
            simtitles = {};
            pulse_energies = [];
            for s = sort(steps.selected)
                sel = get_selection(d, s);
                fullnames{end+1} = sel.fullname;
            end
            graph_time_vs_angle_spread(fullnames, sel.simtitle, sel.pulse_energy, graph_settings, d);
        end          
    end

    function spectrum
        %TODO: Plot this as tgt. thickness (or some chosen variable) vs. absorption
        graph_settings = vt.settings{vt.selected};
        for d = sort(dirs.selected)
            for s = sort(steps.selected)
                sel = get_selection(d, s);
                graph_settings.by = get_y_size_from_deck(sel.fulldir);
                graph_spectrum(sel.fullname, ...
                    sel.simtitle, ...
                    sel.pulse_energy, ...
                    graph_settings);
            end
        end        
    end

    function spectrum_e
        %TODO: Plot this as tgt. thickness (or some chosen variable) vs. absorption
        graph_settings = vt.settings{vt.selected};
        for d = sort(dirs.selected)
            for s = sort(steps.selected)
                sel = get_selection(d, s);
                graph_settings.by = get_y_size_from_deck(sel.fulldir);
                graph_spectrum_e(sel.fullname, ...
                    sel.simtitle, ...
                    sel.pulse_energy, ...
                    graph_settings, ...
                    d);
            end
        end        
    end

    function time_vs_angle_spread_e
        graph_settings = vt.settings{vt.selected};
        for d = sort(dirs.selected)
            fullnames = {};
            simtitles = {};
            pulse_energies = [];
            for s = sort(steps.selected)
                sel = get_selection(d, s);
                fullnames{end+1} = sel.fullname;
            end
            graph_time_vs_angle_spread_e(fullnames, sel.simtitle, sel.pulse_energy, graph_settings, d);
        end          
    end

    function ion_efficiency
        graph_settings = vt.settings{vt.selected};
        arg_name = readable_params(graph_settings.plot_against,1);
        for s = sort(steps.selected)
            disp('      ---      ---      ---      ');
            args = [];
            thicknesses = [];
            total_energy_tt = zeros(0,2);
            energy_in_range_tt = zeros(0,2);
            low_energy_tt = zeros(0,2);
            high_energy_tt = zeros(0,2);
            max_energy_tt = zeros(0,2);
            pulse_energy_tt = [];
            i = 0;
            for d = sort(dirs.selected)
                i = i + 1;
                sel = get_selection(d, s);
                graph_settings.by = get_y_size_from_deck(sel.fulldir);
                deck = load_input_deck(sel.fulldir);
                thicknesses(i) = get_input_deck_param(deck, 'foil_thickness_um');
                if strcmp(arg_name{1}, 'none')
                    args(i) = i;
                else
                    args(i) = get_input_deck_param(deck, arg_name);
                end
                disp(['Argument ' arg_name{1} ' = ' num2str(args(i))]);
                absorption = get_ion_efficiency(sel.fullname, ...
                    sel.simtitle, ...
                    sel.pulse_energy, ...
                    graph_settings, ...
                    thicknesses(i));
                total_energy_tt(i,:) = absorption.total_energy;
                energy_in_range_tt(i,:) = absorption.energy_in_range;
                low_energy_tt(i,:) = absorption.low_energy;
                max_energy_tt(i,:) = absorption.max_energy;
                high_energy_tt(i,:) = absorption.high_energy;
                pulse_energy_tt(i) = absorption.pulse_energy;
            end
            
            [args, sort_index] = sort(args);
          
            try_tag = 1; %TODO: Remove leftover for photon function
            
            total_energy = total_energy_tt(:,try_tag);
            energy_in_range = energy_in_range_tt(:,try_tag);
            low_energy = low_energy_tt(:,try_tag);
            max_energy = max_energy_tt(:,try_tag);
            high_energy = high_energy_tt(:,try_tag);

            pulse_energy = pulse_energy_tt(sort_index);

            total_energy = total_energy(sort_index)';
            energy_in_range = energy_in_range(sort_index)';
            low_energy = low_energy(sort_index)';
            max_energy = max_energy(sort_index)';
            high_energy = high_energy(sort_index)';

            total_conversion = total_energy ./ pulse_energy * 100;
            conversion_in_range = energy_in_range ./ pulse_energy * 100;
            low_conversion = low_energy ./ pulse_energy * 100;
            high_conversion = high_energy ./ pulse_energy * 100;

            mx = max(max_energy);

            if graph_settings.bar_graph
                figure;

                cats  = {   [' < ' num2str(graph_settings.emin) ' MeV'], ...
                            [num2str(graph_settings.emin) ' - ' num2str(graph_settings.emax) ' MeV'], ...
                            [num2str(graph_settings.emax) ' - ' num2str(round(mx, 0)) ' MeV'], ...
                            [num2str(graph_settings.emin) ' - ' num2str(round(mx, 0)) ' MeV'] };
                h = bar([low_conversion; conversion_in_range; high_conversion; total_conversion]);
                set(gca, 'XTick', 1:4, 'XTickLabel', cats);
                ylabel('Conversion efficiency [%]');
                title(rad_type);
                lnames = cellstr(num2str(args(:)))';
                legend(h, lnames);
                legend('show');
                hold off;
                drawnow;
            else
                figure;
                hold on;
                %TODO: Rounding in settings
                if strcmp(arg_name{1}, 'none')
                    line_f = 'o';
                    line_l = 'o';
                else
                    line_f = '-o';
                    line_l = ':o';
                end
                plot(args, total_conversion, line_f, 'LineWidth', 2, 'MarkerSize', 10, ...
                    'DisplayName', [num2str(graph_settings.emin) ' - ' num2str(round(mx, 0)) ' MeV']);
                plot(args, conversion_in_range, line_f, 'LineWidth', 2, 'MarkerSize', 10, ...
                    'DisplayName', [num2str(graph_settings.emin) ' - ' num2str(graph_settings.emax) ' MeV']);
                plot(args, high_conversion, line_f, 'LineWidth', 2, 'MarkerSize', 10, ...
                    'DisplayName', [num2str(graph_settings.emax) ' - ' num2str(round(mx, 0)) ' MeV']);
                plot(args, low_conversion, line_l, 'LineWidth', 2, 'MarkerSize', 10, ...
                    'DisplayName', [' < ' num2str(graph_settings.emin) ' MeV']);
                xlabel(readable_params(graph_settings.plot_against,2));
                ylabel('Conversion efficiency [%]');
                title('Conversion to ions');
                legend('show');
                hold off;
                drawnow;
            end
        end        
    end

    function energy_balance
        graph_settings = vt.settings{vt.selected};
        for d = sort(dirs.selected)
            fullnames = {};
            simtitles = {};
            pulse_energies = [];
            for s = sort(steps.selected)
                sel = get_selection(d, s);
                graph_settings.by = get_y_size_from_deck(sel.fulldir);
                fullnames{end+1} = sel.fullname;
            end
            graph_energy_balance(fullnames, sel.simtitle, sel.pulse_energy, graph_settings, d);
        end          
    end


    function eta
        graph_settings = vt.settings{vt.selected};
        fig = 0;
        if (~graph_settings.display)
            fig = figure;
            drawnow;
        end
        for d = sort(dirs.selected)
            fullnames = {};
            for s = sort(steps.selected)
                sel = get_selection(d, s);
                fullnames{end+1} = sel.fullname;
            end
            graph_settings.by = get_y_size_from_deck(sel.fulldir);
            graph_eta(fullnames, ...
                sel.simtitle, ...
                sel.pulse_energy, ...
                graph_settings, ...
                d, ...
                [settings.outdir '/eta/' sel.dirname], ...
                fig);
        end        
    end


end
