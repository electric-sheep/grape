function s=init_database

% from EPOCH energy output
p1e21 = 4.807293886531387e+05;
p3e21 = 1.442188165959417e+06;
p5e21 = 2.403646943265695e+06;     % 5 x 10^21
p1e22 = 4.807293886531390e+06;     % 1 x 10^22

p3e22 = 1.442188165959417e+07;     % 3.1 x 10^22
p5e22 = 2.403646943265685e+07;
p1e23 = 4.807293886531371e+07;

p170fs = 1.318103325937819e+07;    % 0.5 x 10^22
p135fs = 1.6017e+07;         % 1 x 10^22

% integrated from field files
p1e22_1  = 1693342.30476;
p1e22_2  = 3397908.62726;
p1e22_15 = 2548023.26539;
p1e22_3  = 5097126.79795;
p1e22_06 = 949666.799429;
p1e22_07 = 1148385.04762;
p1e22_08 = 1337070.68217;




s = {
{'r27-30fs-foil-01u',  '0.1 \mum flat foil CH, I = 1\times10^{22} W/cm^2', p1e22}
{'r28-30fs-foil-05u-1022',  '0.5 \mum flat foil CH, I = 1\times10^{22} W/cm^2', p1e22}
{'r29-30fs-foil-1u-1022',  '1 \mum flat foil CH, I = 1\times10^{22} W/cm^2', p1e22}
{'r30-30fs-foil-5u-1022', '5 \mum flat foil CH, I = 1\times10^{22} W/cm^2', p1e22}
{'r31-30fs-foil-10u-1022', '10 \mum flat foil CH, I = 1\times10^{22} W/cm^2', p1e22}
{'r32-30fs-ramp-1u-f1u-1022', '1\mum CH, 1\mum pre-plasma, I = 1\times 10^{22} W/cm^2', p1e22}
{'r33-30fs-ramp-5u-f1u-1022', '1\mum CH, 5\mum pre-plasma, I = 1\times 10^{22} W/cm^2', p1e22}
{'r34-30fs-ramp-10u-f1u-1022', '1\mum CH, 10\mum pre-plasma, I = 1\times 10^{22} W/cm^2', p1e22}
{'r37-135fs-foil-5u-1022', '5 \mum flat foil CH, I = 1\times10^{22} W/cm^2, 135 fs', p135fs }
{'r38-170fs-foil-5u-1022', '5 \mum flat foil CH, I = 0.5\times10^{22} W/cm^2, 170 fs', p170fs }
{'r39-30fs-sph-f1u-1022', '1\mum CH, 500 nm spheres, I = 1\times 10^{22} W/cm^2', p1e22}
{'r40-30fs-foil-2u-1022', '2 \mum flat foil CH, I = 1\times10^{22} W/cm^2', p1e22}
{'r44-30fs-foil-1u-3x1022', '1\mum flat foil CH, I = 3.1\times 10^{22} W/cm^2', p3e22}
{'r45-30fs-ramp-1u-f1u-3x1022', '1\mum CH, 1\mum pre-plasma, I = 3.1\times 10^{22} W/cm^2', p3e22}
{'r46-30fs-ramp-5u-f1u-3x1022', '1\mum CH, 5\mum pre-plasma, I = 3.1\times 10^{22} W/cm^2', p3e22}
{'r47-30fs-ramp-10u-f1u-3x1022', '1\mum CH, 10\mum pre-plasma, I = 3.1\times 10^{22} W/cm^2', p3e22}
{'r48-30fs-1u-5x1021', '5 \mum flat foil CH, I = 5\times10^{21} W/cm^2', p5e21}
{'r53-30fs-foil-2u-foam-20u-correct', '2\mum CH, 20\mum foam, I = 1\times 10^{22} W/cm^2', p1e22}
{'r54-135fs-foil-20u-1022', '20 \mum flat foil CH, I = 1\times10^{22} W/cm^2, 135 fs', p135fs }
{'r55-30fs-wedge-3um-f5u', '5 \mum foil CH, 3\mum wedge, I = 1\times10^{22} W/cm^2', p1e22}
{'r60-30fs-foil-2u-5x1021', '2 \mum flat foil CH, I = 5\times10^{21} W/cm^2', p5e21}
{'r61-30fs-foil-2u-1021', '2 \mum flat foil CH, I = 1\times10^{21} W/cm^2', p1e21}
{'r63-30fs-foil-2u-3x1022', '2 \mum flat foil CH, I = 3\times10^{22} W/cm^2', p3e22}
{'r64-30fs-foil-2u-1022-abr', 'ABR, 2 \mum flat foil CH, I = 1\times10^{22} W/cm^2', p1e22}
{'r65-30fs-foil-2u-1022-haug', 'Haug, 2 \mum flat foil CH, I = 1\times10^{22} W/cm^2', p1e22}
{'r66-30fs-foil-2u-1022-rutherford', 'Kramers, 2 \mum flat foil CH, I = 1\times10^{22} W/cm^2', p1e22}
{'r67-30fs-foil-2u-1022-koch3cn', 'Koch 3CN, 2 \mum flat foil CH, I = 1\times10^{22} W/cm^2', p1e22}
{'r68-30fs-foil-2u-1022-bsonly', 'BS only, 2 \mum flat foil CH, I = 1\times10^{22} W/cm^2', p1e22}
{'r69-30fs-foil-2u-1022-csonly', 'CS only, 2 \mum flat foil CH, I = 1\times10^{22} W/cm^2', p1e22}
{'r70-30fs-al-foil-2u-1022', '2 \mum flat foil Al, I = 1\times10^{22} W/cm^2', p1e22}
{'r71-30fs-al-foil-2u-1022-bsonly', 'BS only, 2 \mum flat foil Al, I = 1\times10^{22} W/cm^2', p1e22}
{'r72-30fs-al-foil-2u-1022-csonly', 'CS only, 2 \mum flat foil Al, I = 1\times10^{22} W/cm^2', p1e22}
{'r73-30fs-al-foil-2u-5x1021', '2 \mum flat foil Al, I = 5\times10^{21} W/cm^2', p5e21}
{'r74-30fs-al-foil-2u-3x1022', '2 \mum flat foil Al, I = 3\times10^{22} W/cm^2', p3e22}
{'r75-30fs-foil-2u-1022-nophotons', 'No photons, 2 \mum flat foil CH, I = 1\times10^{22} W/cm^2', p1e22}
{'r76-30fs-foil-15u-1022', '15 \mum flat foil CH, I = 1\times10^{22} W/cm^2', p1e22}
{'r77-30fs-foil-2u-5x1022', '2 \mum flat foil CH, I = 5\times10^{22} W/cm^2', p5e22}
{'r78-30fs-foil-2u-1023', '2 \mum flat foil CH, I = 1\times10^{23} W/cm^2', p1e23}
{'r79-30fs-foil-2u-1022-wider', '2 \mum flat foil CH, wide y, I = 1\times10^{22} W/cm^2', p1e22}
{'r80-30fs-foil-2u-1022-periodic', '2 \mum flat foil CH, I = 1\times10^{22} W/cm^2, periodic b.c.', p1e22}
{'r82-30fs-foil-2u-1022-orig-epoch', '2 \mum flat foil CH, I = 1\times10^{22} W/cm^2, orig. epoch', p1e22}
{'r84-30fs-al-foil-2u-1022-wide-combi', 'reduced 2 \mum flat foil Al, I = 1\times10^{22} W/cm^2', p1e22}
{'r89-30fs-al-foil-2u-5x1022', '2 \mum flat foil Al, I = 5\times10^{22} W/cm^2', p5e22}
{'r90-30fs-al-foil-2u-1023', '2 \mum flat foil Al, I = 1\times10^{23} W/cm^2', p1e23}
{'r91-30fs-al-foil-2u-3x1021', '2 \mum flat foil Al, I = 3\times10^{21} W/cm^2', p3e21}
{'r92-30fs-al-foil-1u-1022',  '1 \mum flat foil Al, I = 1\times10^{22} W/cm^2', p1e22}
{'r93-30fs-al-foil-5u-1022',  '5 \mum flat foil Al, I = 1\times10^{22} W/cm^2', p1e22}
{'r94-30fs-al-foil-10u-1022',  '10 \mum flat foil Al, I = 1\times10^{22} W/cm^2', p1e22}
{'r95-30fs-al-foil-15u-1022',  '15 \mum flat foil Al, I = 1\times10^{22} W/cm^2', p1e22}
{'r96-30fs-al-foil-2u-1022-xtra-reduce', 'extra reduced 2 \mum flat foil Al, I = 1\times10^{22} W/cm^2', p1e22}
{'r97-30fs-al-foil-2u-1022-low-n-guards', '2 \mum flat foil Al, I = 1\times10^{22} W/cm^2', p1e22}
{'r98-30fs-al-foil-2u-1022-no-guards', 'no guards 2 \mum flat foil Al, I = 1\times10^{22} W/cm^2', p1e22}
{'r99-30fs-foil-2u-1022-wider-csonly', 'CS only, wider, 2 \mum flat foil CH, I = 1\times10^{22} W/cm^2', p1e22}
{'r100-30fs-foil-2u-1022-csonly-run2', 'CS only, wider, run 2, 2 \mum flat foil CH, I = 1\times10^{22} W/cm^2', p1e22}
{'r101-30fs-foil-2u-1022-csonly-orig-epoch', 'CS only, orig. epoch, 2 \mum flat foil CH, I = 1\times10^{22} W/cm^2', p1e22}
{'r102-30fs-foil-2u-1022-wider-csonly-orig-epoch', 'CS only, wider, orig. epoch 2 \mum flat foil CH, I = 1\times10^{22} W/cm^2', p1e22}
{'r103-30fs-al-foil-5u-1022-run2',  '2 \mum flat foil Al, run 2, I = 1\times10^{22} W/cm^2', p1e22}
{'r104-30fs-al-foil-2um-small-csonly-run1', '2 \mum Al flat foil, I = 1\times10^{22} W/cm^2, SMALL, 100 CPWL', p1e22}
{'r105-30fs-al-foil-2um-small-csonly-run2', '2 \mum Al flat foil, I = 1\times10^{22} W/cm^2, SMALL, 100 CPWL', p1e22}
{'r106-30fs-al-foil-2um-small-csonly-run3', '2 \mum Al flat foil, I = 1\times10^{22} W/cm^2, SMALL, 100 CPWL', p1e22}
{'r107-30fs-al-foil-2um-small-csonly-200cpwl', '2 \mum Al flat foil, I = 1\times10^{22} W/cm^2, SMALL, 200 CPWL', p1e22}
{'r108-30fs-al-foil-2um-small-csonly-300cpwl', '2 \mum Al flat foil, I = 1\times10^{22} W/cm^2, SMALL, 300 CPWL', p1e22}
{'r109-30fs-al-foil-2um-small-csonly-run4', '2 \mum Al flat foil, I = 1\times10^{22} W/cm^2, SMALL, 100 CPWL', p1e22}
{'r110-30fs-al-foil-2um-small-csonly-run5', '2 \mum Al flat foil, I = 1\times10^{22} W/cm^2, SMALL, 100 CPWL', p1e22}
{'r111-30fs-al-foil-2um-small-csonly-run6', '2 \mum Al flat foil, I = 1\times10^{22} W/cm^2, SMALL, 100 CPWL', p1e22}
{'r112-30fs-al-foil-2um-small-csonly-run7', '2 \mum Al flat foil, I = 1\times10^{22} W/cm^2, SMALL, 100 CPWL', p1e22}
{'r113-30fs-al-foil-2um-small-csonly-run8', '2 \mum Al flat foil, I = 1\times10^{22} W/cm^2, SMALL, 100 CPWL', p1e22}
{'r114-30fs-al-foil-2um-small-csonly-run9', '2 \mum Al flat foil, I = 1\times10^{22} W/cm^2, SMALL, 100 CPWL', p1e22}
{'r115-30fs-al-foil-2um-small-csonly-run10', '2 \mum Al flat foil, I = 1\times10^{22} W/cm^2, SMALL, 100 CPWL', p1e22}
{'r120-30fs-al-foil-20u-1022', '20 \mum Al flat foil, I = 1\times10^{22} W/cm^2', p1e22}
{'r121-30fs-al-foil-5u-1022-smalldom', '5 \mum Al flat foil, I = 1\times10^{22} W/cm^2', p1e22}
{'r122-30fs-au-foil-5u-1022-smalldom', '5 \mum Au flat foil, I = 1\times10^{22} W/cm^2', p1e22}
{'r123-30fs-al-foil-2u-1022-smalldom', '2 \mum Al flat foil, I = 1\times10^{22} W/cm^2', p1e22}
{'r124-30fs-au-foil-2u-1022-smalldom', '2 \mum Au flat foil, I = 1\times10^{22} W/cm^2', p1e22}
{'r129-30fs-ch-foil-1u-1022', '1 \mum CH flat foil, I = 1\times10^{22} W/cm^2', p1e22}
{'r131-30fs-al-foil-2u-pp-1u-1022', '2 \mum Al + 1\mum exp. pre-plasma, I = 1\times10^{22} W/cm^2', p1e22}
{'r134-30fs-au30+-foil-5u-1022-smalldom',  '5 \mum Au 30+ flat foil, I = 1\times10^{22} W/cm^2', p1e22}
{'r135-30fs-ch-foil-1u-1022-moving-photons', '1 \mum CH, moving photons, I = 1\times10^{22} W/cm^2', p1e22}
{'r137-30fs-ch-foil-5u-pp-1u-1022', '5 \mum CH -1 \mum PP, , I = 1\times10^{22} W/cm^2', p1e22}
{'r138-30fs-ch-foil-5u-pp-4u-1022', '5 \mum CH -4 \mum PP, , I = 1\times10^{22} W/cm^2', p1e22}
{'r140-30fs-ch-foil-1u-1022-tight-moving-photons', '2 \mum CH, moving photons, tight focusing, I = 1\times10^{22} W/cm^2', p1e22_1}
{'t001-30fs-3u-1022-ch-2u', '2 \mum CH, w_0 = 3 \mum, I = 1\times10^{22} W/cm^2, m. ph.', p1e22_3}
{'t002-30fs-1u-1022-ch-2u', '2 \mum CH, w_0 = 1 \mum, I = 1\times10^{22} W/cm^2, m. ph.', p1e22_1}
{'t004-30fs-1u-1022-ch-2u-p1u', '2 \mum CH 1 \mum p.p., w_0 = 1 \mum, I = 1\times10^{22} W/cm^2, m. ph.', p1e22_1}
{'t005-30fs-06u-1022-ch-2u', '2 \mum CH, w_0 = 0.6 \mum, I = 1\times10^{22} W/cm^2, m. ph.', p1e22_06}
{'t005-30fs-06u-1022-ch-2u-run2', '2 \mum CH, w_0 = 0.6 \mum, I = 1\times10^{22} W/cm^2, m. ph., RUN 2', p1e22_06}
{'t005-30fs-06u-1022-ch-2u-run3', '2 \mum CH, w_0 = 0.6 \mum, I = 1\times10^{22} W/cm^2, m. ph., RUN 3', p1e22_06}
{'t006-30fs-1u-1022-ch-2u-p05u', '2 \mum CH 0.5 \mum p.p., w_0 = 1 \mum, I = 1\times10^{22} W/cm^2, m. ph.', p1e22_1}
{'t008-30fs-1u-foc+2u-1022-ch-2u', '2 \mum CH, w_0 = 1 \mum, I = 1\times10^{22} W/cm^2, x_f = +2 \mum, m. ph.', p1e22_1}
{'t009-30fs-1u-foc-2u-1022-ch-2u', '2 \mum CH, w_0 = 1 \mum, I = 1\times10^{22} W/cm^2, x_f = -2 \mum, m. ph.', p1e22_1}
{'t010-30fs-1u-foc+1u-1022-ch-2u', '2 \mum CH, w_0 = 1 \mum, I = 1\times10^{22} W/cm^2, x_f = +1 \mum, m. ph.', p1e22_1}
{'t011-30fs-1u-foc-1u-1022-ch-2u', '2 \mum CH, w_0 = 1 \mum, I = 1\times10^{22} W/cm^2, x_f = -1 \mum, m. ph.', p1e22_1}
{'t012-30fs-1u-1022-ch-2u-p01u', '2 \mum CH 0.1 \mum p.p., w_0 = 1 \mum, I = 1\times10^{22} W/cm^2, m. ph.', p1e22_1}
{'t013-30fs-3u-1022-ch-02u', '0.2 \mum CH, w_0 = 3 \mum, I = 1\times10^{22} W/cm^2, m. ph.', p1e22_3}
{'t014-30fs-3u-1022-ch-03u', '0.3 \mum CH, w_0 = 3 \mum, I = 1\times10^{22} W/cm^2, m. ph.', p1e22_3}
{'t015-30fs-2u-1022-ch-2u', '2 \mum CH, w_0 = 2 \mum, I = 1\times10^{22} W/cm^2, m. ph.', p1e22_2}
{'t016-30fs-1u5-1022-ch-2u', '2 \mum CH, w_0 = 1.5 \mum, I = 1\times10^{22} W/cm^2, m. ph.', p1e22_15}
{'t017-30fs-08u-1022-ch-2u', '2 \mum CH, w_0 = 0.8 \mum, I = 1\times10^{22} W/cm^2, m. ph.', p1e22_08}
{'t018-30fs-3u-3x1021-ch-2u', '2 \mum CH, w_0 = 3 \mum, I = 3\times10^{21} W/cm^2, m. ph.', p3e21}
{'t019-30fs-3u-5x1021-ch-2u', '2 \mum CH, w_0 = 3 \mum, I = 5\times10^{21} W/cm^2, m. ph.', p5e21}
{'t020-30fs-3u-3x1022-ch-2u', '2 \mum CH, w_0 = 3 \mum, I = 3\times10^{22} W/cm^2, m. ph.', p3e22}
{'t021-30fs-3u-5x1022-ch-2u', '2 \mum CH, w_0 = 3 \mum, I = 5\times10^{22} W/cm^2, m. ph.', p5e22}
{'t022-30fs-3u-1023-ch-2u', '2 \mum CH, w_0 = 3 \mum, I = 1\times10^{23} W/cm^2, m. ph.', p1e23}
{'t023-30fs-1u-foc+05u-1022-ch-2u', '2 \mum CH, w_0 = 1 \mum, I = 1\times10^{22} W/cm^2, x_f = +0.5 \mum, m. ph.', p1e22_1}
{'t024-30fs-1u-foc-05u-1022-ch-2u', '2 \mum CH, w_0 = 1 \mum, I = 1\times10^{22} W/cm^2, x_f = -0.5 \mum, m. ph.', p1e22_1}
{'t025-30fs-07u-1022-ch-2u', '2 \mum CH, w_0 = 0.7 \mum, I = 1\times10^{22} W/cm^2, m. ph.', p1e22_07}
{'tx002-run01', 'RUN 01: 2 \mum CH, w_0 = 1 \mum, I = 1\times10^{22} W/cm^2, m. ph.', p1e22_1}
{'tx002-run02', 'RUN 02: 2 \mum CH, w_0 = 1 \mum, I = 1\times10^{22} W/cm^2, m. ph.', p1e22_1}
{'tx002-run03', 'RUN 03: 2 \mum CH, w_0 = 1 \mum, I = 1\times10^{22} W/cm^2, m. ph.', p1e22_1}
{'tx002-run04', 'RUN 04: 2 \mum CH, w_0 = 1 \mum, I = 1\times10^{22} W/cm^2, m. ph.', p1e22_1}
{'tx002-run05', 'RUN 05: 2 \mum CH, w_0 = 1 \mum, I = 1\times10^{22} W/cm^2, m. ph.', p1e22_1}
{'tx002-run06', 'RUN 06: 2 \mum CH, w_0 = 1 \mum, I = 1\times10^{22} W/cm^2, m. ph.', p1e22_1}
{'tx002-run07', 'RUN 07: 2 \mum CH, w_0 = 1 \mum, I = 1\times10^{22} W/cm^2, m. ph.', p1e22_1}
{'tx002-run08', 'RUN 08: 2 \mum CH, w_0 = 1 \mum, I = 1\times10^{22} W/cm^2, m. ph.', p1e22_1}
{'tx002-run09', 'RUN 09: 2 \mum CH, w_0 = 1 \mum, I = 1\times10^{22} W/cm^2, m. ph.', p1e22_1}
{'tx002-run10', 'RUN 10: 2 \mum CH, w_0 = 1 \mum, I = 1\times10^{22} W/cm^2, m. ph.', p1e22_1}
{'tx002-run11', 'RUN 11: 2 \mum CH, w_0 = 1 \mum, I = 1\times10^{22} W/cm^2, m. ph.', p1e22_1}
{'tx002-run12', 'RUN 12: 2 \mum CH, w_0 = 1 \mum, I = 1\times10^{22} W/cm^2, m. ph.', p1e22_1}
{'tx002-run13', 'RUN 13: 2 \mum CH, w_0 = 1 \mum, I = 1\times10^{22} W/cm^2, m. ph.', p1e22_1}
{'tx002-run14', 'RUN 14: 2 \mum CH, w_0 = 1 \mum, I = 1\times10^{22} W/cm^2, m. ph.', p1e22_1}
{'tx002-run15', 'RUN 15: 2 \mum CH, w_0 = 1 \mum, I = 1\times10^{22} W/cm^2, m. ph.', p1e22_1}
{'tx002-run16', 'RUN 16: 2 \mum CH, w_0 = 1 \mum, I = 1\times10^{22} W/cm^2, m. ph.', p1e22_1}
{'tx002-run17', 'RUN 17: 2 \mum CH, w_0 = 1 \mum, I = 1\times10^{22} W/cm^2, m. ph.', p1e22_1}
{'tx002-run18', 'RUN 18: 2 \mum CH, w_0 = 1 \mum, I = 1\times10^{22} W/cm^2, m. ph.', p1e22_1}
{'tx002-run19', 'RUN 19: 2 \mum CH, w_0 = 1 \mum, I = 1\times10^{22} W/cm^2, m. ph.', p1e22_1}
{'tx002-run20', 'RUN 20: 2 \mum CH, w_0 = 1 \mum, I = 1\times10^{22} W/cm^2, m. ph.', p1e22_1}
{'temp-test/c-abr', 'CH - ABR', 1} 
{'temp-test/c-haug', 'CH - Haugh', 1} 
{'temp-test/c-rutherford', 'CH - Rutherford', 1} 
{'temp-test/c-koch', 'CH - Koch', 1} 
{'temp-test/c-pen', 'CH - Seltzer & Berger', 1} 
{'temp-test/c-pen2', 'CH - Seltzer & Berger, randomized order', 1} 
{'temp-test/abr', 'Au - ABR', 1} 
{'temp-test/haug', 'Au - Haugh', 1} 
{'temp-test/rutherford', 'Au - Rutherford', 1} 
{'temp-test/koch', 'Au - Koch 3CN', 1}
{'temp-test/koch-scr', 'Au - Koch 3CS(a)', 1}
{'temp-test/penelope', 'Au - Seltzer & Berger', 1} 
{'temp-test/penelope-angle', 'Au - Seltzer & Berger + angles', 1} 
{'temp-test/test-noangle', 'TEST SIMULATION parallel', 1}
{'temp-test/test-angle', 'TEST SIMULATION dipole', 1}
{'temp-test', 'TEST SIMULATION', 1}
{'omg', 'TEST SIMULATION', 1}
};

end