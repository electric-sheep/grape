function graph_eta(filenames, simtitle, pulse_energy_j, settings, simid, outdir, fig)

    smoothing = settings.smoothing;
    emax_in = settings.emax;
    angle_restriction = settings.angle_restriction;
    eln_ratio = settings.eln_ratio;
    
    integrated = settings.integrated;
    display = settings.display;

    boundary_y = settings.boundary_y * 1e-6;
    
    xmin = settings.xmin * 1e-6;
    xmax = settings.xmax * 1e-6;
    
    omit_y = [settings.by(1)+boundary_y, settings.by(2)-boundary_y];
    
    export_results = settings.export_results;
    
    scale_factor = 1e-12;

    el_chg = 1.6021766208e-19;    
    
    
    
    emax = emax_in;
    emin = 0;

    nbins = max(2,floor((emax-emin)/smoothing));  % Draw at least two bins in case emax-emin < 2*smoothing
    nbins = min(100000, nbins);                  % Cap number of bins to 100 000 (Matlab can't ahndle "too many" bins)
    disp(['nbins = ' num2str(nbins)]);
    ebin_size = (emax-emin)/nbins;
    hist_n = emin:ebin_size:emax;
    hfinal = zeros([1 length(hist_n)-1]);
    
    
    n=0;

    for f=filenames

        n = n+1;

        l=GetDataSDF(f{1});
        disp(['Processing ' f{1}]);

        if isfield(l.Grid.Particles, 'subset_elns')
            subset_name = 'subset_elns';
        elseif isfield(l.Grid.Particles, 'subset_elns_hi')
            subset_name = 'subset_elns_hi';
        elseif isfield(l.Grid.Particles, 'subset_elnsf')
            subset_name = 'subset_elnsf';
        else
            continue;
        end

        electron_species = fieldnames(l.Grid.Particles.(subset_name));

        m_e = 9.10938291e-31;

        x = []; y = []; px = []; py = []; pz = []; w = []; eta = [];

        time = l.time;
        step = l.step;

        for i=1:numel(electron_species)
            x = [x; l.Grid.Particles.(subset_name).(electron_species{i}).x];
            y = [y; l.Grid.Particles.(subset_name).(electron_species{i}).y];
            px = [px; l.Particles.Px.(subset_name).(electron_species{i}).data];
            py = [py; l.Particles.Py.(subset_name).(electron_species{i}).data];
            pz = [pz; l.Particles.Pz.(subset_name).(electron_species{i}).data];
            w = [w; l.Particles.Weight.(subset_name).(electron_species{i}).data];
            eta = [eta; l.Particles.Eta.(subset_name).(electron_species{i}).data];
        end

        disp(['Number of macroparticles '  num2str(size(w,1)) ]);

        %electron_weight = 0.5*wmax*photon_multiplier;
        %number_of_electrons = 625000;
        %scale_factor_bs = 1/number_of_electrons/electron_weight;

        if display
            fig = figure;   % Set up a new figure
        else
            clf(fig, 'reset');  % Clear the current figure
        end

        hold on;

        if angle_restriction < 90
            idx_dir = find_in_direction(1, angle_restriction, px, py);
        else
            idx_dir = find_in_direction(0, 90, px, py);
        end
        idx_all = find(y>omit_y(1) & y<omit_y(2) & x>xmin & x<xmax);
        idx = intersect(idx_dir, idx_all);


        % Weighted histogram, msg. 7 at (acumarray fails with zeros):
        % https://www.mathworks.com/matlabcentral/newsreader/view_thread/265558,
        [n, edges, bin]= histcounts(eta(idx), hist_n);
        h = zeros(size(n));
        wsel = w(idx);
        npart = length(wsel);
        for i = 1:npart
            if bin(i) > 0
                h(bin(i)) = h(bin(i)) + wsel(i);
                hfinal(bin(i)) = hfinal(bin(i)) + wsel(i);
            end
        end

        p = plot(hist_n(1:end-1), scale_factor*h, 'LineWidth', 2, 'DisplayName', ['e^-, ' simtitle ', t = ' num2str(1e15*time) ' fs']);
        set(gca, 'YScale', 'log');

        disp(['Number of electrons: ' num2str(npart) ]);

        legend('show', 'Location', 'SouthOutside', 'Orientation', 'Horizontal');
        title(['e^- \eta histogram: ' simtitle ', t = ' num2str(time)]);
        xlim([emin emax]);

        hold off;
        drawnow;

        if ~exist(outdir, 'dir')
            mkdir(outdir);
        end
        fname = [outdir '/eta_' num2str(step, '%09d')];
        saveas(gcf, [fname '.png'], 'png');
        saveas(gcf, [fname '.fig'], 'fig');
    end
    
    if integrated
        if display
            fig = figure;   % Set up a new figure
        else
            clf(fig, 'reset');  % Clear the current figure
        end
        hold on;
        p = plot(hist_n(1:end-1), scale_factor*hfinal, 'LineWidth', 2, 'DisplayName', ['e^-, ' simtitle ', t = ' num2str(1e15*time) ' fs']);
        set(gca, 'YScale', 'log');

        legend('show', 'Location', 'SouthOutside', 'Orientation', 'Horizontal');
        title(['e^- \eta histogram, time integrated: ' simtitle ', t = ' num2str(time)]);
        xlim([emin emax]);

        hold off;
        drawnow;
    end
    
    
end