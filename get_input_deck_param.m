function val = get_input_deck_param(deck, param)

    % \< matches beginnig of a word (newline or space)
    % \s* whitespace
    % (-?\d+\.?\d*[eE]?\d*) is a number possibly negative, and possibly in scientific format
    % \s*\*\s*(\w*) might by multiplied by a word
    pattern = strcat('\<', param, '\s*=\s*(?<number>-?\d+\.?\d*[eE]?\d*)\s*\*?\s*(?<unit>\w*)');
    names = regexp(deck, pattern, 'names');
    
    if iscell(names)
        tokens = names{1};
    elseif isstruct(names)
        if size(names,2) > 1
            tokens = names(1);
        else
            tokens = names;
        end
    else
        disp('Unknown type of match tokens: ');
        disp(names);
    end
    
    val = str2double(tokens.number);
    
    if strcmp(tokens.unit, 'micro') || strcmp(tokens.unit, 'micron')
        val = val * 1e-6;
    elseif strcmp(tokens.unit, 'nano')
        val = val * 1e-9;
    elseif strcmp(tokens.unit, 'pico')
        val = val * 1e-12;
    elseif strcmp(tokens.unit, 'femto')
        val = val * 1e-15;
    end
end
