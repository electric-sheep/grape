function graph_time_vs_energy(filenames, simtitle, pulse_energy_j, settings, simid)

    px_old = [];
    py_old = [];
    pz_old = [];

    emin = settings.emin;
    ebin_size = settings.ebin_size;
    n_ebins = settings.n_ebins;
    emax = emin + (n_ebins-1)*ebin_size;
    all_e = [0, emin:ebin_size:emax, inf];

    export_results = settings.export_results;
    differential = settings.differential;
    cumulative = settings.cumulative;
    angle_restriction = settings.angle_restriction;
    round_n = settings.round;
    phot_ratio = 1;

    directions = [];
    if settings.backward == 1
        directions(end+1) = -1;
    end
    if settings.forward == 1
        directions(end+1) = 1;
    end
    if settings.both == 1
        directions(end+1) = 0;
    end
    
    disp(['Processing simulation ' simtitle]);
    el_chg = 1.6021766208e-19;
    pulse_energy = pulse_energy_j / el_chg / 1e6; % From 'n' file, in MeV
    disp(['Pulse energy: ' num2str(pulse_energy_j) ' J = ' num2str(pulse_energy, '%10.3e') ' MeV']);

    
    nsteps = size(filenames,2);

    energies_cumulative = zeros(2, 3, n_ebins+1, nsteps);
    energies_differential = zeros(2, 3, n_ebins+1, nsteps);
    times = zeros(1, nsteps);

    n = 0;

    max_ene = zeros(2,3);

    
    for f=filenames
        %TODO: Sort and partition by energy instead of traversing the whole
        %      array for every energy bin
        n = n + 1;

        l=GetDataSDF(f{1});
        
        if ~isfield(l, 'Particles')
            continue
        end
        
        px = l.Particles.Px.subset_phot.photon.data;
        py = l.Particles.Py.subset_phot.photon.data;
        pz = l.Particles.Pz.subset_phot.photon.data;
        ene = sqrt(px.^2+py.^2+pz.^2)*3e8/1.602e-19/1e6;  % in MeV

        w = l.Particles.Weight.subset_phot.photon.data;
        tag = l.Particles.Tag.subset_phot.photon.data;

        times(1,n) = l.time;

        for try_tag = 1:2
            if try_tag == 1
                if settings.cs == 0
                    continue
                end
                rad_type = 'CS';
            elseif try_tag == 2
                if settings.bs == 0
                    continue
                end
                rad_type = 'BS';
            end

            idx_tag = find(tag==try_tag);
            
            for lr = directions
                idx_dir = find_in_direction(lr, angle_restriction, px, py);
                idx_all = intersect(idx_dir, idx_tag);
                if any(idx_all)
                    max_ene_now = max(ene(idx_all));
                    disp(['t = ' num2str(l.time) ', ' rad_type num2str(lr) ': Emax = ' num2str(max_ene_now)]);
                    max_ene(try_tag, lr+2) = max(max_ene_now, max_ene(try_tag, lr+2));
                    for i = 1:size(all_e,2)-1   
                        idx = intersect(find(ene>all_e(i) & ene<all_e(i+1)), idx_all);
                        energies_cumulative(try_tag, lr+2, i, n) = sum(ene(idx).*w(idx)) / phot_ratio; % nasobeni vahou je spravne - jeden "1 MeV" foton je ve skutecnosti w "1 MeV" fotonu
                    end
                else
                    disp(['t = ' num2str(l.time) ', no ' rad_type num2str(lr) ' photons']);
                end
            end
        end

    end

    for try_tag = 1:2
        if try_tag == 1
            if settings.cs == 0
                continue
            end
            rad_type = 'CS';
        elseif try_tag == 2
            if settings.bs == 0
                continue
            end
            rad_type = 'BS';
        end

        draw_graph(try_tag, rad_type);
    end
    
    export_variables();

    
    function draw_graph(tag_number, rad_name)
        
        if angle_restriction < 90
            angle_text = [' +- ' num2str(angle_restriction) ' deg'];
        else
            angle_text = [];
        end
        
        for lr = directions
            
            ncurves = size(energies_cumulative, 3);
            
            if cumulative == 1
                figure;
                hold on;
                for i = 1:ncurves
                    mx = all_e(i+1);
                    if isinf(mx)
                        mx = max_ene(tag_number, lr+2);
                    end
                    rat = i/ncurves;
                    if i == 1
                        lsp = '--';
                    else
                        lsp = '-';
                    end                    
                    plot(times, squeeze(energies_cumulative(tag_number,lr+2,i,:)), ...
                        lsp, 'LineWidth', 2, 'Color', [rat   0.5*lr+0.5  0.5*rat*(tag_number-1)], 'DisplayName', ...
                        [num2str(round(all_e(i),round_n)) ' - ' num2str(round(mx,round_n)) ' MeV']);
                end
                title([rad_name ', ' simtitle ', ' direction_name(lr) angle_text]);
                legend('show');
                xlabel('Time [s]');
                ylabel('Energy in photons [MeV m^{-1}]');
                xlim([min(times) max(times)]);
                hold off;
                drawnow;
            end

            if differential == 1
                for i = 1:ncurves
                    energies_differential(tag_number, lr+2, i, :) = ...
                        [squeeze(energies_cumulative(tag_number,lr+2,1,1)), squeeze(diff(energies_cumulative(tag_number,lr+2,i,:)))'];
                end
                figure;
                hold on;
                for i = 1:ncurves
                    mx = all_e(i+1);
                    if isinf(mx)
                        mx = max_ene(tag_number, lr+2);
                    end
                    rat = i/ncurves;
                    if i == 1
                        lsp = '--';
                    else
                        lsp = '-';
                    end
                    plot(times, squeeze(energies_differential(tag_number,lr+2,i,:)), ...
                        lsp, 'LineWidth', 2, 'Color', [rat   0.5*lr+0.5  0.5*rat*(tag_number-1)], 'DisplayName', ...
                        [num2str(round(all_e(i),round_n)) ' - ' num2str(round(mx,round_n)) ' MeV']);
                end
                title([rad_name ' differential, ' simtitle ', ' direction_name(lr) angle_text]);
                legend('show');
                xlabel('Time [s]');
                ylabel('Energy in photons per \Delta t [MeV m^{-1} (10 fs)^{-1}]');
                xlim([min(times) max(times)]);
                hold off;
                drawnow;
            end
        end
    end

    function export_variables
        if export_results
            assignin('base', ['times_' num2str(simid)], times);
            if cumulative == 1
                assignin('base', ['energies_cumulative_' num2str(simid)], energies_cumulative);
            end
            if differential == 1
                assignin('base', ['energies_differential_' num2str(simid)], energies_differential);
            end
        end
    end

end

