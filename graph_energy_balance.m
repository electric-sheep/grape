function graph_energy_balance(filenames, simtitle, pulse_energy_j, settings, simid)

    el_chg = 1.6021766208e-19;
    e0  = 8.854187817e-12;
    mu0 = 1.2566370614e-6;

    eln_ratio = settings.eln_ratio;
    ion_ratio = settings.ion_ratio;
    phot_ratio = settings.phot_ratio;

    boundary_y = settings.boundary_y * 1e-6;
    omit_y = [settings.by(1)+boundary_y, settings.by(2)-boundary_y];
    export_results = settings.export_results;

    
    disp(['Processing simulation ' simtitle]);
    pulse_energy = pulse_energy_j / el_chg / 1e6; % From 'n' file, in MeV
    disp(['Pulse energy: ' num2str(pulse_energy_j) ' J = ' num2str(pulse_energy, '%10.3e') ' MeV']);

    nsteps = size(filenames,2);

    field_energy = zeros(1, nsteps);
    electrons_energy = zeros(1, nsteps);
    ions_energy = zeros(1, nsteps);
    photons_energy = zeros(1, nsteps);
    times = zeros(1, nsteps);

    n = 0;

    max_ene = zeros(4);

    
    
    for f=filenames
        
        n = n + 1;

        orig_name = f{1};
        dump_suffix = orig_name(end-7:end);
        path = orig_name(1:end-10);
        fname = [path 'f' dump_suffix];
        l=GetDataSDF(fname);
        disp(['Processing ' fname]);
        
        times(n) = l.time;
        
        boundary_thickness = l.Boundary_thickness;
        dx = l.Grid.Grid.x(2) - l.Grid.Grid.x(1);
        dy = l.Grid.Grid.y(2) - l.Grid.Grid.y(1);
        dz = dx;
        xb = boundary_thickness;
        yb = boundary_thickness + floor(boundary_y/dy);
        
        Ex = l.Electric_Field.Ex.data(xb:end-xb,yb:end-yb);
        Ey = l.Electric_Field.Ey.data(xb:end-xb,yb:end-yb);
        Bz = l.Magnetic_Field.Bz.data(xb:end-xb,yb:end-yb);
        
        ene = dx*dy* 0.5 * ( e0 * (Ex.^2+Ey.^2) + 1/mu0 * Bz.^2 ) /el_chg/1e6;
        
        total_ene = sum(sum(ene));
        disp(['E = ' num2str(total_ene)]);
        field_energy(n) = total_ene;
        
        for particle_type = {'elns', 'gold', 'phot'}
            ptype = particle_type{1};
            switch ptype
                case 'elns'
                    fname = [path 'pe' dump_suffix];
                    ratio = eln_ratio;
                    m = 9.109e-31;
                case 'alum'
                    fname = [path 'pa' dump_suffix];
                    ratio = ion_ratio;
                    m = 49184.3*9.109e-31;
                case 'prot'
                    fname = [path 'pa' dump_suffix];
                    ratio = ion_ratio;
                    m = 1836*9.109e-31;
                case 'gold'
                    fname = [path 'pa' dump_suffix];
                    ratio = ion_ratio;
                    m = 362400*9.109e-31;
                case 'carb'
                    fname = [path 'pc' dump_suffix];
                    ratio = ion_ratio;
                    m = 12*1836*9.109e-31;                    
                case 'phot'
                    fname = [path 'pf' dump_suffix];
                    ratio = phot_ratio;
            end
            
            disp(['Processing ' fname]);
            
            l=GetDataSDF(fname);

            if ~isfield(l, 'Particles')
                total_ene = 0;
            else
                species = fieldnames(l.Particles.Weight.(['subset_' ptype]));

                y = []; px = []; py = []; pz = []; w = [];

                for i=1:numel(species)
                    y = [y; l.Grid.Particles.(['subset_' ptype]).(species{i}).y];
                    px = [px; l.Particles.Px.(['subset_' ptype]).(species{i}).data];
                    py = [py; l.Particles.Py.(['subset_' ptype]).(species{i}).data];
                    pz = [pz; l.Particles.Pz.(['subset_' ptype]).(species{i}).data];
                    w = [w; l.Particles.Weight.(['subset_' ptype]).(species{i}).data];
                end
    
                if ptype == 'phot' 
                    ene=sqrt(px.^2+py.^2+pz.^2)*3e8 /el_chg/1e6;  % in MeV
                else
                    ene=m*3e8^2*(1/(m*3e8)*sqrt((m*3e8)^2+px.^2+py.^2+pz.^2)-1) /el_chg/1e6;  % in MeV
                end

                idx_all = y>omit_y(1) & y<omit_y(2);
                
                total_ene = sum(ene(idx_all).*w(idx_all)) / ratio;
            end

            disp(['E = ' num2str(total_ene)]);
            
            switch ptype
                case 'elns'
                    electrons_energy(n) = total_ene;
                case 'alum'
                    ions_energy(n) = total_ene;
                case 'prot'
                    ions_energy(n) = total_ene;
                case 'carb'
                    ions_energy(n) = total_ene;
                case 'gold'
                    ions_energy(n) = total_ene;
                case 'phot'
                    photons_energy(n) = total_ene;
            end

        end

    end
    
    draw_graph();
    
    export_variables();

    function draw_graph()
        figure;
        hold on;
        
        plot(times, field_energy, 'LineWidth', 2, 'DisplayName', 'EM fields');
        plot(times, electrons_energy, 'LineWidth', 2, 'DisplayName', 'Electrons');
        plot(times, ions_energy, 'LineWidth', 2, 'DisplayName', 'Ions');
        plot(times, photons_energy, 'LineWidth', 2, 'DisplayName', 'Photons');
        plot(times, field_energy+electrons_energy+ions_energy+photons_energy, ...
            'LineWidth', 2, 'DisplayName', 'Total');

        title(['Energy balance, ' simtitle]);
        legend('show');
        xlabel('Time [s]');
        ylabel('Energy [MeV]');
        xlim([min(times) max(times)]);
        
        hold off;
        drawnow;
    end

    function export_variables
        if export_results
            assignin('base', ['times_' num2str(simid)], times);
            assignin('base', ['field_energy_' num2str(simid)], field_energy);
            assignin('base', ['electrons_energy_' num2str(simid)], electrons_energy);
            assignin('base', ['ions_energy_' num2str(simid)], ions_energy);
            assignin('base', ['photons_energy_' num2str(simid)], photons_energy);
        end
    end

end

