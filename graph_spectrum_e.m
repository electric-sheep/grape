function graph_spectrum_e(filename, simtitle, pulse_energy_j, settings, simid)

    disp(['Simulation: ' simtitle ', processing file: ' filename]);

    smoothing = settings.smoothing;
    emin = settings.emin;
    emax_in = settings.emax;
    angle_restriction = settings.angle_restriction;
    eln_ratio = settings.eln_ratio;
    
    boundary_y = settings.boundary_y * 1e-6;
    
    xmin = settings.xmin * 1e-6;
    xmax = settings.xmax * 1e-6;
    
    omit_y = [settings.by(1)+boundary_y, settings.by(2)-boundary_y];
    
       
    export_results = settings.export_results;
    
    scale_factor = 1e-12;

    l=GetDataSDF(filename);

    el_chg = 1.6021766208e-19;

    if isfield(l.Grid.Particles, 'subset_elns')
        subset_name = 'subset_elns';
    elseif isfield(l.Grid.Particles, 'subset_elns_hi')
        subset_name = 'subset_elns_hi';
    elseif isfield(l.Grid.Particles, 'subset_elnsf')
        subset_name = 'subset_elnsf';
    end
    
    electron_species = fieldnames(l.Grid.Particles.(subset_name));
    
    m_e = 9.10938291e-31;
    
    x = []; y = []; px = []; py = []; pz = []; w = [];

    time = l.time;
    step = l.step;
    
    for i=1:numel(electron_species)
        x = [x; l.Grid.Particles.(subset_name).(electron_species{i}).x];
        y = [y; l.Grid.Particles.(subset_name).(electron_species{i}).y];
        px = [px; l.Particles.Px.(subset_name).(electron_species{i}).data];
        py = [py; l.Particles.Py.(subset_name).(electron_species{i}).data];
        pz = [pz; l.Particles.Pz.(subset_name).(electron_species{i}).data];
        w = [w; l.Particles.Weight.(subset_name).(electron_species{i}).data];
    end

    disp(['Number of macroparticles '  num2str(size(w,1)) ]);
    
    ene = m_e*3e8^2*(1/(m_e*3e8)*sqrt((m_e*3e8)^2+px.^2+py.^2+pz.^2)-1) /el_chg/1e6;   % in MeV

    max_ene = max(ene);
    emax = min(max_ene,emax_in);
    if ~isfinite(emax)
        emax = max_ene;
    end
    disp(['Max energy: ' num2str(emax) ' MeV'])

    pulse_energy = pulse_energy_j / el_chg / 1e6; % From 'n' file, in MeV
    disp(['Pulse energy: ' num2str(pulse_energy_j) ' J = ' num2str(pulse_energy, '%10.3e') ' MeV']);


    nbins = max(2,floor((emax-emin)/smoothing));  % Draw at least two bins in case emax-emin < 2*smoothing
    nbins = min(100000, nbins);                  % Cap number of bins to 100 000 (Matlab can't handle "too many" bins)
    disp(['nbins = ' num2str(nbins)]);
    ebin_size = (emax-emin)/nbins; % approx. 1 MeV bins if not capped.

    %electron_weight = 0.5*wmax*photon_multiplier;
    %number_of_electrons = 625000;
    %scale_factor_bs = 1/number_of_electrons/electron_weight;

    figure;
    hold on;

    if angle_restriction < 90
        idx_dir = find_in_direction(1, angle_restriction, px, py);
    else
        idx_dir = find_in_direction(0, 90, px, py);
    end
    idx_all = find(y>omit_y(1) & y<omit_y(2) & x>xmin & x<xmax & ene>emin & ene<emax);
    idx = intersect(idx_dir, idx_all);

    % Weighted histogram, msg. 7 at (acumarray fails with zeros):
    % https://www.mathworks.com/matlabcentral/newsreader/view_thread/265558,
    hist_n = emin:ebin_size:emax;
    [n, edges, bin]= histcounts(ene(idx), hist_n);
    h = zeros(size(n));
    wsel = w(idx);
    npart = length(wsel);
    for i = 1:npart
        if bin(i) > 0
            h(bin(i)) = h(bin(i)) + wsel(i);
        end
    end

    p = plot(hist_n(1:end-1), scale_factor*h, 'LineWidth', 2, 'DisplayName', ['e^-, ' simtitle ', t = ' num2str(1e15*time) ' fs']);
    set(gca, 'YScale', 'log');

    total_energy = sum(ene(idx).*w(idx)) / eln_ratio;

    disp(['Total energy (e-): ' num2str(total_energy, '%10.3e')]);
    disp(['Conversion efficiency (e-):  ' num2str(total_energy/pulse_energy*100) '%']);
    
    disp(['Number of macroparticles (e-): ' num2str(npart) ]);


    legend('show', 'Location', 'SouthOutside', 'Orientation', 'Horizontal');
    title(['e^- energy spectrum: ' simtitle]);

    hold off;
    drawnow;

    export_variables();

    function export_variables
        if export_results
            assignin('base', ['electron_spectrum_energy_r' num2str(simid) '_t' num2str(step)], h);
            assignin('base', ['electron_spectrum_number_r' num2str(simid) '_t' num2str(step)], hist_n(1:end-1));
            assignin('base', ['electron_spectrum_scale_factor_r' num2str(simid) '_t' num2str(step)], scale_factor);
        end
    end

end
