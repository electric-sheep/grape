function graph_time_vs_energy_detector(filenames, simtitle, pulse_energy_j, settings, simid)

    px_old = [];
    py_old = [];
    pz_old = [];

    emin = settings.emin;
    ebin_size = settings.ebin_size;
    n_ebins = settings.n_ebins;
    emax = emin + (n_ebins-1)*ebin_size;
    all_e = [0, emin:ebin_size:emax, inf];

    export_results = settings.export_results;
    angle_restriction = settings.angle_restriction;
    round_n = settings.round;
    phot_ratio = 1;
    detector_position = settings.detector_position_um * 1e-6;
    
    directions = [];
    if settings.backward == 1
        directions(end+1) = -1;
    end
    if settings.forward == 1
        directions(end+1) = 1;
    end
    if settings.both == 1
        directions(end+1) = 0;
    end
    
    disp(['Processing simulation ' simtitle]);
    el_chg = 1.6021766208e-19;
    pulse_energy = pulse_energy_j / el_chg / 1e6; % From 'n' file, in MeV
    disp(['Pulse energy: ' num2str(pulse_energy_j) ' J = ' num2str(pulse_energy, '%10.3e') ' MeV']);

    
    nsteps = size(filenames,2);

    energies = zeros(2, 3, n_ebins+1, nsteps);
    times = zeros(1, nsteps);

    n = 0;

    max_ene = zeros(2,3);

    tflight = [];
    ene_here = [];
    w_here = [];
    tag_here = [];
    
    for f=filenames
        %TODO: Sort and partition by energy instead of traversing the whole
        %      array for every energy bin
        n = n + 1;

        l=GetDataSDF(f{1});

        x = l.Grid.Particles.subset_phot.photon.x;
        px = l.Particles.Px.subset_phot.photon.data;
        py = l.Particles.Py.subset_phot.photon.data;
        pz = l.Particles.Pz.subset_phot.photon.data;
        ene = sqrt(px.^2+py.^2+pz.^2)*3e8/1.602e-19/1e6;  % in MeV

        w = l.Particles.Weight.subset_phot.photon.data;
        tag = l.Particles.Tag.subset_phot.photon.data;

        times(1,n) = l.time;

        for try_tag = 1:2
            if try_tag == 1
                if settings.cs == 0
                    continue
                end
                rad_type = 'CS';
            elseif try_tag == 2
                if settings.bs == 0
                    continue
                end
                rad_type = 'BS';
            end

            idx_tag = find(tag==try_tag);
            
            for lr = directions
                %TODO: This will wrongly remove photons with exact
                %      same momentum...
                pxdiff = ~ismember(px, px_old);
                pydiff = ~ismember(py, py_old);
                pzdiff = ~ismember(pz, pz_old);
                iarr_dif = pxdiff | pydiff | pzdiff;
                
                idx_dir = find_in_direction(lr, angle_restriction, px, py);
                idx_all_dir = intersect(idx_dir, idx_tag);
                idx_all = intersect(find(iarr_dif), idx_all_dir);
                
                if any(idx_all)
                    max_ene_now = max(ene(idx_all));
                    disp(['t = ' num2str(l.time) ', ' rad_type num2str(lr) ': Emax = ' num2str(max_ene_now)]);
                    max_ene(try_tag, lr+2) = max(max_ene_now, max_ene(try_tag, lr+2));
                    for i = 1:size(all_e,2)-1   
                        idx = intersect(find(ene>all_e(i) & ene<all_e(i+1)), idx_all);
                        
                        ene_here = [ene_here; ene(idx)];
                        w_here = [w_here; w(idx)];
                        tag_here = [tag_here; tag(idx)];
                        
                        tflight = [tflight;  ( (detector_position-x(idx))/3e8 + l.time ) ];
                    end
                else
                    disp(['t = ' num2str(l.time) ', no ' rad_type num2str(lr) ' photons']);
                end
            end
        end
        
        px_old = px;
        py_old = py;
        pz_old = pz;
        
    end

    for try_tag = 1:2
        if try_tag == 1
            if settings.cs == 0
                continue
            end
            rad_type = 'CS';
        elseif try_tag == 2
            if settings.bs == 0
                continue
            end
            rad_type = 'BS';
        end
        
        if angle_restriction < 90
            angle_text = [' +- ' num2str(angle_restriction) ' deg'];
        else
            angle_text = [];
        end
        
        
        for lr = directions
            if any(idx_all)
                figure; hold on;
                title([rad_type ', ' simtitle ', ' direction_name(lr) angle_text]);
                
                max_ene(try_tag, lr+2) = max(max_ene_now, max_ene(try_tag, lr+2));
                for i = 1:size(all_e,2)-1   
                    idx = find(ene_here>all_e(i) & ene_here<all_e(i+1));
    
                    hist_n = min(tflight(idx)):10e-15:max(tflight(idx));
                    [n, edges, bin]= histcounts(tflight(idx), hist_n);
                    number = zeros(size(n));
                    total_ene = zeros(size(n));
                    wsel = w_here(idx);
                    esel = ene_here(idx);
                    npart = length(wsel);
                    for j = 1:npart
                        if bin(j) > 0
                            number(bin(j)) = number(bin(j)) + wsel(j);
                            total_ene(bin(j)) = total_ene(bin(j)) + ene_here(j).*wsel(j);
                        end
                    end

                    %p = plot(hist_n(1:end-1), number, 'LineWidth', 2, 'DisplayName', ...
                    %    [num2str(round(all_e(i),round_n)) '+  MeV']);
                    p = plot(hist_n(1:end-1), total_ene, 'LineWidth', 2, 'DisplayName', ...
                        [num2str(round(all_e(i),round_n)) '+  MeV']);
                end
                legend('show');
                xlabel('Time [s]');
                %ylabel('Total number of photons [arb. u.]');
                ylabel('Total energy in photons per \Delta t [MeV m^{-1} (10 fs)^{-1}]');
            else
                disp(['t = ' num2str(l.time) ', no ' rad_type num2str(lr) ' photons']);
            end
        end
        drawnow;
    end
    
    export_variables();

    
    function export_variables
        if export_results
            assignin('base', ['times_' num2str(simid)], times);
            if cumulative == 1
                assignin('base', ['energies_cumulative_' num2str(simid)], energies);
            end
            if differential == 1
                assignin('base', ['energies_differential_' num2str(simid)], energies_differential);
            end
        end
    end

end

