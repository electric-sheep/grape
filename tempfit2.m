function [ fE ] = tempfit2(E, n, T0in)

    if nargin < 3
        T0 = [1e21, 1e25];
    else
        T0 = T0in;
    end
    
    kB = 1.38064852e-23;
    
    function y = f(Ei, T, j)
        y = 2*sqrt(Ei/pi) .* (1/(kB*T(j)))^(3/2) .* exp(-Ei/(kB*T(j)));
    end

    function y = df(Ei, T, j)
        y = 1/kB^(3/2) * sqrt(Ei/pi) .* ( 2*Ei/kB*T(j)^(-7/2) - 3*T(j)^(-5/2) ) .* exp(-Ei/(kB*T(j)));
    end

    function y = S(Ei, ni, T, j)
        function fs = fsum(Ei)
            fs = 0;
            for jj = 1:length(T)
                fs = fs + f(Ei, T, jj);
            end
        end
        yi = 2 * df(Ei, T, j) .* (ni - fsum(Ei));
        y = sum(yi);
    end

    %fE = f(E, T0, 1) + f(E, T0, 2);
    
    fun = @(T) [S(E, [T(0), T(1)], 1), S(E, [T(0), T(1)], 2)];
    
    [T, resnorm, ff, exitflag, output, jacob] = newtonraphson(fun, T0')

end

