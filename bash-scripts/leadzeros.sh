#!/bin/bash

base=$PWD

for dir in $@; do
    cd $base/$dir
    for prefix in px_ py_; do
        for f in ${prefix}[0-9][0-9][0-9][0-9].png; do 
            mv "${f}" "${f/${prefix}/${prefix}0}"
        done
    done
done

cd $base

