#!/bin/bash

base=$PWD

for dir in $@; do
    echo "Processing $base/$dir"

    cd $base/$dir

    echo "Stitching x vs. px+py"
    for x in x_px_*.png; do 
        n=`echo ${x} | grep -o '[0-9]*'`
        y="x_py_${n}.png"
        convert ${x} ${y} -append "x_pxpy_${n}.png"
    done
    
    echo "Stitching y vs. px+py"
    for x in y_px_*.png; do 
        n=`echo ${x} | grep -o '[0-9]*'`
        y="y_py_${n}.png"
        convert ${x} ${y} -append "y_pxpy_${n}.png"
    done

    echo "Stitching x+y vs. px+py"    
    for x in x_pxpy_*.png; do 
        n=`echo ${x} | grep -o '[0-9]*'`
        y="y_pxpy_${n}.png"
        convert ${x} ${y} +append "xy_pxpy_${n}.png"
    done
       
done

cd $base

