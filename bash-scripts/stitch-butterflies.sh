#!/bin/bash

base=$PWD
rad="BS CS"

for dir in $@; do
    echo "Processing $base/$dir"

    cd $base/$dir

    echo "Stitching cumulative + differential plots"
    for r in $rad; do
        for cc in $(ls cropped_cumulative_$r_*.png); do 
            n=$(echo ${cc} | grep -o '[0-9]*')
            dd="cropped_differential_${r}_${n}.png"
            if [ -f ${dd} ]; then
                convert ${cc} ${dd} +append "stitched_${r}_${n}.png"
            fi
        done
    done
       
done

cd $base

