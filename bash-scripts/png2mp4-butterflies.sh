#!/bin/bash

base=$PWD

filetypes="differential_BS differential_CS cumulative_CS cumulative_BS stitched_BS stitched_CS"

for dir in $@; do
    dir=${dir%/}    # strip trailing slash
    fullpath=$base/$dir
    if [[ -d $fullpath ]]; then
        echo -e "\n---\nProcessing $fullpath\n---\n"
        for ft in $filetypes; do
            cd $fullpath
            
            glob="${ft}_*.png"
            echo -e "\n---\nGlob: ${glob}\n---\n"
            
            if ls $glob 1> /dev/null 2>&1; then
                ffmpeg -framerate 2 -pattern_type glob -i "$glob" ../${dir}-${ft}.mp4
            else
                echo "$glob not found"
            fi
        done
    fi
done

cd $base

