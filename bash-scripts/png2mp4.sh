#!/bin/bash

base=$PWD

velocities="x_px x_py x_pxpy y_pxpy xy_pxpy"

for dir in $@; do
    dir=${dir%/}    # strip trailing slash
    fullpath=$base/$dir
    if [[ -d $fullpath ]]; then
        echo -e "\n---\nProcessing $fullpath\n---\n"
        for vel in $velocities; do
            cd $fullpath
            
            glob="${vel}_*.png"
            echo "\n---\nGlob: ${glob}\n---\n"
            
            if ls $glob 1> /dev/null 2>&1; then
                ffmpeg -framerate 3 -pattern_type glob -i "$glob" ../${dir}-${vel}.mp4
            else
                echo "$glob not found"
            fi
        done
    fi
done

cd $base

