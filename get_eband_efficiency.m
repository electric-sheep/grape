function absorption = get_eband_efficiency(filename, simtitle, pulse_energy_j, settings)

emin = settings.emin;
emax = settings.emax;
phot_ratio = settings.phot_ratio;  % photons in output / all simulated photons

boundary_y = settings.boundary_y * 1e-6;
omit_y = [settings.by(1)+boundary_y, settings.by(2)-boundary_y];

angle_restriction = settings.angle_restriction;

use_tags = settings.tags;

disp(['Simulation: ' simtitle ', processing file: ' filename]);

l=GetDataSDF(filename);

el_chg = 1.6021766208e-19;

x=l.Grid.Particles.subset_phot.photon.x;
y=l.Grid.Particles.subset_phot.photon.y;
px=l.Particles.Px.subset_phot.photon.data;
py=l.Particles.Py.subset_phot.photon.data;
pz=l.Particles.Pz.subset_phot.photon.data;
ene=sqrt(px.^2+py.^2+pz.^2)*3e8/1.602e-19/1e6;  % in MeV
w=l.Particles.Weight.subset_phot.photon.data;

if use_tags == 1
    tag = l.Particles.Tag.subset_phot.photon.data;
    tags_to_try = 1:2;
else
    tags_to_try = 0;
end

pulse_energy = pulse_energy_j / el_chg / 1e6; % From 'n' file, in MeV

total_energy = zeros(2,1);
energy_in_range = zeros(2,1);
low_energy = zeros(2,1);
max_energy = zeros(2,1);
high_energy = zeros(2,1);

for try_tag = tags_to_try
    [rad_type, rad_idx, is_enabled] = rad_info_from_tag(try_tag, settings);
    if is_enabled == 0
        continue
    end

    idx = find(y>omit_y(1) & y<omit_y(2));
    if try_tag > 0
        idx = intersect(find(tag==rad_idx), idx);
    end
    if angle_restriction < 90
        idx = intersect(find_in_direction(1, angle_restriction, px, py), idx);
    end
    if any(idx)
        max_energy(rad_idx) = max(ene(idx));

        idx_total = intersect(find(ene>=emin), idx);
        total_energy(rad_idx) = sum(ene(idx_total).*w(idx_total)) / phot_ratio;

        idx_low = intersect(find(ene<emin), idx);
        low_energy(rad_idx) = sum(ene(idx_low).*w(idx_low)) / phot_ratio;

        idx_range = intersect(find(ene<=emax), idx_total);
        energy_in_range(rad_idx) = sum(ene(idx_range).*w(idx_range)) / phot_ratio;

        idx_high = intersect(find(ene>=emax), idx_total);
        high_energy(rad_idx) = sum(ene(idx_high).*w(idx_high)) / phot_ratio;
        
        disp(['Pulse energy: ' num2str(pulse_energy_j) ' J = ' num2str(pulse_energy, '%10.3e') ' MeV']);

        disp(['Total energy ' rad_type ' > ' num2str(emin) ' MeV: ' num2str(total_energy(rad_idx), '%10.3e') ...
              '   in ' num2str(emin) '-' num2str(emax) ' MeV: ' num2str(energy_in_range(rad_idx), '%10.3e')])

        disp(['Conversion efficiency ' rad_type ' > ' num2str(emin) ' MeV:  ' num2str(total_energy(rad_idx)/pulse_energy) ...
              ',   in ' num2str(emin) '-' num2str(emax) ' MeV: ' num2str(energy_in_range(rad_idx)/pulse_energy)])

        disp(['Low energy ' rad_type ' < ' num2str(emin) ' MeV: '  num2str(low_energy(rad_idx), '%10.3e') ...
              ' MeV. Conversion efficiency: ' num2str(low_energy(rad_idx)/pulse_energy)]);

        disp(['High energy ' rad_type ' > ' num2str(emax) ' MeV: '  num2str(high_energy(rad_idx), '%10.3e') ...
              ' MeV. Conversion efficiency: ' num2str(high_energy(rad_idx)/pulse_energy)]);
              
    else
        disp(['No ' rad_type ' photons in selected region']);
        max_energy(rad_idx) = 0;
        total_energy(rad_idx) = 0;
        low_energy(rad_idx) = 0;
        energy_in_range(rad_idx) = 0;
        high_energy(rad_idx) = 0;
    end
end

absorption = struct(...
    'pulse_energy_j', pulse_energy_j, ...
    'pulse_energy', pulse_energy, ...
    'total_energy', total_energy, ...
    'energy_in_range', energy_in_range, ...
    'low_energy', low_energy, ...
    'high_energy', high_energy, ...
    'max_energy', max_energy);

end

