function y = get_y_size_from_deck(simpath)
    deck = load_input_deck(simpath);
    y = zeros(1,2);
    y(1) = get_input_deck_param(deck, 'y_min');
    y(2) = get_input_deck_param(deck, 'y_max');
end

