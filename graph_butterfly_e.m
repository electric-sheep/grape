function stats = graph_butterfly_e(filename, simtitle, pulse_energy_j, settings)

abin_size = settings.abin_size;
eln_ratio = settings.eln_ratio;

emin = settings.emin;
ebin_size = settings.ebin_size;
n_ebins = settings.n_ebins;
emax = emin + (n_ebins-1)*ebin_size;

disp(['Simulation: ' simtitle ', processing file: ' filename]);
stats = struct();

l=GetDataSDF(filename);

el_chg = 1.6021766208e-19;
m_e = 9.10938291e-31;

x = l.Grid.Particles.subset_elns.electron.x;
y = l.Grid.Particles.subset_elns.electron.y;

px=l.Particles.Px.subset_elns.electron.data;
py=l.Particles.Py.subset_elns.electron.data;
pz=l.Particles.Pz.subset_elns.electron.data;
ene=m_e*3e8^2*(1/(m_e*3e8)*sqrt((m_e*3e8)^2+px.^2+py.^2+pz.^2)-1) /el_chg/1e6;  % in MeV

stats.max_energy = max(ene);

w = l.Particles.Weight.subset_elns.electron.data;

pulse_energy = pulse_energy_j / el_chg / 1e6; % From 'n' file, in MeV
disp(['Pulse energy: ' num2str(pulse_energy_j) ' J = ' num2str(pulse_energy, '%10.3e') ' MeV']);

alpha=atan2(py,px)/pi*180;
angles=1:abin_size:360;
neg_ang = find(alpha<1);
alpha(neg_ang) = alpha(neg_ang)+360;

figure;
polaraxes('ThetaZeroLocation', 'right'); 
hold on;

for ie=emin:ebin_size:emax
    h = zeros(size(angles));
    for i = 1:size(angles,2)
        ang = angles(i);
        idx = find(alpha >= ang & alpha < ang+abin_size & ene>ie & ene<ie+ebin_size);
        s = sum(ene(idx).*w(idx));
        h(i) = (s/pulse_energy) / (abin_size*eln_ratio);
    end
    p = polarplot(h,'LineWidth', 2, 'DisplayName', [num2str(ie) ' - ' num2str(ie+ebin_size) ' MeV']);
end

total_energy = sum(ene(idx).*w(idx)) / eln_ratio;
idx_mm = find(ene>emin & ene<emax);
energy_in_mm = sum(ene(idx_mm).*w(idx_mm)) / eln_ratio;

disp(['Total energy: ' num2str(total_energy, '%10.3e') '   in ' num2str(emin) '-' num2str(emax) ' MeV: ' num2str(energy_in_mm, '%10.3e')])
disp(['Conversion efficiency:  ' num2str(total_energy/pulse_energy*100) '%   in ' num2str(emin) '-' num2str(emax) ' MeV: ' num2str(energy_in_mm/pulse_energy*100) '%'])            

legend('show', 'Location', 'SouthOutside', 'Orientation', 'Horizontal');
title(['Electrons: ' simtitle]);
hold off; 
drawnow;
end

