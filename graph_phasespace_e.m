function stats = graph_phasespace_e(filename, simtitle, pulse_energy_j, settings, outdir, fig)

eln_ratio = settings.eln_ratio;

resolution = settings.resolution;

display = settings.display;
save = settings.save;

tic
disp(['Simulation: ' simtitle ', processing file: ' filename]);
stats = struct();

l=GetDataSDF(filename);

el_chg = 1.6021766208e-19;
m_e = 9.10938291e-31;
c0 = 3e8;
pnorm=m_e*c0;

electron_species = fieldnames(l.Grid.Particles.subset_elnsf);

x = []; y = []; px = []; py = []; pz = []; ene = []; w = [];

for i=1:numel(electron_species)
x = [x; l.Grid.Particles.subset_elnsf.(electron_species{i}).x];
y = [y; l.Grid.Particles.subset_elnsf.(electron_species{i}).y];

px = [px; l.Particles.Px.subset_elnsf.(electron_species{i}).data];
py = [py; l.Particles.Py.subset_elnsf.(electron_species{i}).data];
pz = [pz; l.Particles.Pz.subset_elnsf.(electron_species{i}).data];

w = [w; l.Particles.Weight.subset_elnsf.(electron_species{i}).data];
end

ene=m_e*3e8^2*(1/(m_e*3e8)*sqrt((m_e*3e8)^2+px.^2+py.^2+pz.^2)-1) /el_chg/1e6;  % in MeV
stats.max_energy = max(ene);

sim_step = l.step;
sim_time = l.time;

pulse_energy = pulse_energy_j / el_chg / 1e6; % From 'n' file, in MeV
disp(['Pulse energy: ' num2str(pulse_energy_j) ' J = ' num2str(pulse_energy, '%10.3e') ' MeV']);

%xmin = min(x); xmax = max(x);
xmin = settings.xmin/1e6; 
xmax = settings.xmax/1e6; 
xstep = (xmax-xmin)/resolution;

%ymin = min(y); ymax = max(y);
ymin = settings.ymin/1e6; 
ymax = settings.ymax/1e6; 
ystep = (ymax-ymin)/resolution;

% Always autocalculate the p limits here, and override them later if neccessary
pxmin = min(px); 
pxmax = max(px); 
pxmax = max(abs(pxmin), abs(pxmax));
pxmin = -pxmax;

pymin = min(py); 
pymax = max(py); 
pymax = max(abs(pymin), abs(pymax));
pymin = -pymax;

pzmin = min(pz); 
pzmax = max(pz);
pzmax = max(abs(pzmin), abs(pzmax));
pzmin = -pzmax;

% Override the p limits if requested 
if isfinite(settings.pmin)
    pxmin = settings.pmin * pnorm;
    pymin = settings.pmin * pnorm;
    pzmin = settings.pmin * pnorm;
end

if isfinite(settings.pmax)
    pxmax = settings.pmax * pnorm;
    pymax = settings.pmax * pnorm;
    pzmax = settings.pmax * pnorm;
end

pxstep = (pxmax-pxmin)/resolution;
pystep = (pymax-pymin)/resolution;
pzstep = (pzmax-pzmin)/resolution;


xax = xmin : xstep : xmax;
yax = ymin : ystep : ymax;
pxax = pxmin : pxstep : pxmax;
pyax = pymin : pystep : pymax;
pzax = pzmin : pzstep : pzmax;

% Scaled variable indexes convert the continuous varibale value range into
% discrete integer values which can be later used as indexes.
xidx = round((x-xmin)./xstep + 1);
yidx = round((y-ymin)./ystep + 1);

pxidx = round((px-pxmin)./pxstep + 1);
pyidx = round((py-pymin)./pystep + 1);
pzidx = round((pz-pzmin)./pzstep + 1);

labels = {'x [\mum]' 'y [\mum]' 'p_x [m_e^{-1}c^{-1}]' 'p_y [m_e^{-1}c^{-1}]' 'p_z [m_e^{-1}c^{-1}]'};
axes_x = {};
labels_x = {};
if settings.x_x
    axes_x(end+1) = {'x'};
    labels_x(end+1) = labels(1);
end
if settings.x_y
    axes_x(end+1) = {'y'};
    labels_x(end+1) = labels(2);
end
if settings.x_px
    axes_x(end+1) = {'px'};
    labels_x(end+1) = labels(3);
end
if settings.x_py
    axes_x(end+1) = {'py'};
    labels_x(end+1) = labels(4);
end
if settings.x_pz
    axes_x(end+1) = {'pz'};
    labels_x(end+1) = labels(5);
end

axes_y = {};
labels_y = {};
if settings.y_x
    axes_y(end+1) = {'x'};
    labels_y(end+1) = labels(1);
end
if settings.y_y
    axes_y(end+1) = {'y'};
    labels_y(end+1) = labels(2);
end
if settings.y_px
    axes_y(end+1) = {'px'};
    labels_y(end+1) = labels(3);
end
if settings.y_py
    axes_y(end+1) = {'py'};
    labels_y(end+1) = labels(4);
end
if settings.y_pz
    axes_y(end+1) = {'pz'};
    labels_y(end+1) = labels(5);
end



for xx = 1:length(axes_x)
    
    switch axes_x{xx}
        case 'x'
            axis_x = xax;
            idx_x = xidx;
            norm_x = 1e6;
            lim_x = [xmin xmax];
        case 'y'
            axis_x = yax;
            idx_x = yidx;
            norm_x = 1e6;
            lim_x = [ymin ymax];
        case 'px'
            axis_x = pxax;
            idx_x = pxidx;
            norm_x = 1/pnorm;
            lim_x = [pxmin pxmax];
        case 'py'
            axis_x = pyax;
            idx_x = pyidx;
            norm_x = 1/pnorm;
            lim_x = [pymin pymax];
        case 'pz'
            axis_x = pzax;
            idx_x = pzidx;
            norm_x = 1/pnorm;
            lim_x = [pzmin pzmax];
    end
    
    for yy = 1:length(axes_y)
        switch axes_y{yy}
            case 'x'
                axis_y = xax;
                idx_y = xidx;
                norm_y = 1e6;
                lim_y = [xmin xmax];
            case 'y'
                axis_y = yax;
                idx_y = yidx;
                norm_y = 1e6;
                lim_y = [ymin ymax];
            case 'px'
                axis_y = pxax;
                idx_y = pxidx;
                norm_y = 1/pnorm;
                lim_y = [pxmin pxmax];
            case 'py'
                axis_y = pyax;
                idx_y = pyidx;
                norm_y = 1/pnorm;
                lim_y = [pymin pymax];
            case 'pz'
                axis_y = pzax;
                idx_y = pzidx;
                norm_y = 1/pnorm;
                lim_y = [pzmin pzmax];
        end
        
        canvas = zeros(length(axis_x), length(axis_y));
        
        for j = 1:length(idx_x)
            xloc = idx_x(j);
            % Scaled index of the particular variable must lie within canvas bounds
            if xloc >=1 && xloc <= size(canvas, 1) && ene(j) >= settings.emin
                yloc = idx_y(j);
                if yloc >=1 && yloc <= size(canvas, 2)
                    canvas(xloc, yloc) = canvas(xloc, yloc) + w(j)/eln_ratio;
                end
            end
        end
        
        flt = fspecial('average', 3);
        
        
        if display
            fig = figure;   % Set up a new figure
        else
            clf(fig, 'reset');  % Clear the current figure
        end

        figure(fig);    % Draw into figure "fig"

        title([simtitle char(10) 'e^- phasespace t = ' num2str(round(sim_time*1e15)) ' fs']);
        hold on;
        filtered = log10(filter2(flt, canvas'));
        filtered(filtered == -Inf) = NaN;
        pcolor(norm_x * axis_x, norm_y * axis_y, filtered);
        shading flat;
        colormap parula;
        
        xlabel(labels_x{xx});
        ylabel(labels_y{yy});
        
        xlim(norm_x *lim_x);
        ylim(norm_y *lim_y);
        
        hold off;

        drawnow;


        if save
            if ~exist(outdir, 'dir')
                mkdir(outdir);
            end
            fname = [outdir '/' axes_x{xx} '_' axes_y{yy} '_' num2str(sim_step, '%09d')];
            saveas(gcf, [fname '.png'], 'png');
            saveas(gcf, [fname '.fig'], 'fig');
        end

        
    end
end

toc

end

