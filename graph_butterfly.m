function stats = graph_butterfly(filenames, simtitle, pulse_energy_j, settings, outdir, fig)

abin_size = settings.abin_size;
phot_ratio = settings.phot_ratio;

butterfly = settings.butterfly;

emin = settings.emin;
ebin_size = settings.ebin_size;
n_ebins = settings.n_ebins;
emax = emin + (n_ebins-1)*ebin_size;

percentiles = settings.percentiles;
percentile_list = [50 75 95 99 100];


differential = settings.differential;
cumulative = settings.cumulative;

display = settings.display;
save = settings.save;

tic
disp(['Simulation: ' simtitle]);
el_chg = 1.6021766208e-19;

stats = struct();

%nsteps = size(filenames,2);

px_old = [];
py_old = [];
pz_old = [];

n=0;

for f=filenames
   
    n = n+1;
    
    l=GetDataSDF(f{1});
    disp(['Processing ' f{1}]);

    if ~isfield(l, 'Particles')
        continue
    end
    
    px=l.Particles.Px.subset_phot.photon.data;
    py=l.Particles.Py.subset_phot.photon.data;
    pz=l.Particles.Pz.subset_phot.photon.data;
    ene=sqrt(px.^2+py.^2+pz.^2)*3e8/1.602e-19/1e6;  % in MeV

    w = l.Particles.Weight.subset_phot.photon.data;
    tag = l.Particles.Tag.subset_phot.photon.data;

    pulse_energy = pulse_energy_j / el_chg / 1e6; % From 'n' file, in MeV
    disp(['Pulse energy: ' num2str(pulse_energy_j) ' J = ' num2str(pulse_energy, '%10.3e') ' MeV']);

    alpha=atan2(py,px)/pi*180;
    angles=1:abin_size:360;
    neg_ang = find(alpha<1);
    alpha(neg_ang) = alpha(neg_ang)+360;

    sim_time = l.time;
    sim_step = l.step;
    
    for try_tag = 1:2
        if try_tag == 1
            if settings.cs == 0
                continue
            end
            rad_type = 'CS';
        elseif try_tag == 2
            if settings.bs == 0
                continue
            end
            rad_type = 'BS';
        end

        if any(tag==try_tag)

            if cumulative == 1
                if display
                    fig = figure;   % Set up a new figure
                else
                    clf(fig, 'reset');  % Clear the current figure
                end

                figure(fig);    % Draw into figure "fig"

                if butterfly
                    polaraxes('ThetaZeroLocation', 'right'); 
                else
                    axes;
                    xlim([0 360]);
                end
                
                hold on;

                if percentiles
                    bins = prctile(ene(ene>1), percentile_list);
                else
                    bins = emin:ebin_size:emax+ebin_size;
                end
                
                
                for ib=1:length(bins)-1
                    h = zeros(size(angles));
                    ebin_min = bins(ib);
                    ebin_max = bins(ib+1);
                    for i = 1:size(angles,2)
                        ang = angles(i);
                        idx = find(alpha >= ang & alpha < ang+abin_size & ene>ebin_min & ene<ebin_max & tag==try_tag);
                        s = sum(ene(idx).*w(idx));
                        h(i) = (s/pulse_energy) / (abin_size*phot_ratio);
                    end
                    if percentiles
                        plot_name = [num2str(ebin_min,2) ' - ' num2str(ebin_max,2) ' MeV, P_{' num2str(percentile_list(ib)) ' - ' num2str(percentile_list(ib+1)) '%}'];
                    else
                        plot_name = [num2str(ebin_min,2) ' - ' num2str(ebin_max,2) ' MeV'];
                    end
                    if butterfly
                        p = polarplot(h,'LineWidth', 2, 'DisplayName', plot_name);
                    else
                        p = plot(angles,h,'LineWidth', 2, 'DisplayName', plot_name);
                    end
                end

                legend('show', 'Location', 'SouthOutside', 'Orientation', 'Horizontal');
                title([simtitle char(10) rad_type ', t = '  num2str(round(sim_time*1e15)) ' fs']);
                hold off; 
                drawnow;

                if save
                    if ~exist(outdir, 'dir')
                        mkdir(outdir);
                    end
                    fname = [outdir '/cumulative_' rad_type '_' num2str(sim_step, '%09d')];
                    saveas(gcf, [fname '.png'], 'png');
                    saveas(gcf, [fname '.fig'], 'fig');
                end
            end
            
            if differential == 1
                if display
                    fig = figure;   % Set up a new figure
                else
                    clf(fig, 'reset');  % Clear the current figure
                end

                figure(fig);    % Draw into figure "fig"

                if butterfly
                    polaraxes('ThetaZeroLocation', 'right'); 
                else
                    axes;
                    xlim([0 360]);
                end
                
                hold on;

                %TODO: This will wrongly remove photons with exact
                %      same momentum...
                pxdiff = ~ismember(px, px_old);
                pydiff = ~ismember(py, py_old);
                pzdiff = ~ismember(pz, pz_old);
                iarr_diff = pxdiff | pydiff | pzdiff;
                
                if percentiles
                    bins = prctile(ene(ene>1), percentile_list);
                else
                    bins = emin:ebin_size:emax+ebin_size;
                end
                
                for ib=1:length(bins)-1
                    h = zeros(size(angles));
                    ebin_min = bins(ib);
                    ebin_max = bins(ib+1);
                    for i = 1:size(angles,2)
                        ang = angles(i);
                        idx = find(alpha >= ang & alpha < ang+abin_size & ene>ebin_min & ene<ebin_max & tag==try_tag & iarr_diff);
                        s = sum(ene(idx).*w(idx));
                        h(i) = (s/pulse_energy) / (abin_size*phot_ratio);
                    end
                    if percentiles
                        plot_name = [num2str(ebin_min) ' - ' num2str(ebin_max,2) ' MeV, P_{' num2str(percentile_list(ib)) ' - ' num2str(percentile_list(ib+1)) '%}'];
                    else
                        plot_name = [num2str(ebin_min) ' - ' num2str(ebin_max,2) ' MeV'];
                    end
                    if butterfly
                        p = polarplot(h,'LineWidth', 2, 'DisplayName', plot_name);
                    else
                        p = plot(angles,h,'LineWidth', 2, 'DisplayName', plot_name);
                    end
                end

                legend('show', 'Location', 'SouthOutside', 'Orientation', 'Horizontal');
                title([simtitle char(10) rad_type ' diff, t = '  num2str(round(sim_time*1e15)) ' fs']);
                hold off; 
                drawnow;

                if save
                    if ~exist(outdir, 'dir')
                        mkdir(outdir);
                    end
                    fname = [outdir '/differential_' rad_type '_' num2str(sim_step, '%09d')];
                    saveas(gcf, [fname '.png'], 'png');
                    saveas(gcf, [fname '.fig'], 'fig');
                end
            end
            
            if cumulative == 1
                idx = find(tag==try_tag);
                total_energy = sum(ene(idx).*w(idx)) / phot_ratio;
                idx_mm = find(ene>emin & ene<emax & tag==try_tag);
                energy_in_mm = sum(ene(idx_mm).*w(idx_mm)) / phot_ratio;

                disp(['Total energy (' rad_type '): ' num2str(total_energy, '%10.3e') '   in ' num2str(emin) '-' num2str(emax) ' MeV: ' num2str(energy_in_mm, '%10.3e')])
                disp(['Conversion efficiency (' rad_type '):  ' num2str(total_energy/pulse_energy*100) '%   in ' num2str(emin) '-' num2str(emax) ' MeV: ' num2str(energy_in_mm/pulse_energy*100) '%'])            
            end
            
        else
            disp(['No ' rad_type ' photons']);
        end
        
    end
    
    px_old = px;
    py_old = py;
    pz_old = pz;
    
end

end

