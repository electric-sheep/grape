function [ rad_type, rad_idx, is_enabled ] = rad_info_from_tag(try_tag, settings)
%RAD_INFO_FROM_TAG Summary of this function goes here
%   Detailed explanation goes here
    
    is_enabled = 1;
    if try_tag == 1
        if settings.cs == 0
            is_enabled = 0;
        end
        rad_type = 'CS';
        rad_idx = try_tag;
    elseif try_tag == 2
        if settings.bs == 0
            is_enabled = 0;
        end
        rad_type = 'BS';
        rad_idx = try_tag;
    elseif try_tag == 0
        if settings.cs == 0
            is_enabled = 0;
        end
        rad_type = 'CS';
        rad_idx = 1;
    end

end

