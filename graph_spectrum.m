function graph_spectrum(filename, simtitle, pulse_energy_j, settings)

disp(['Simulation: ' simtitle ', processing file: ' filename]);

phot_ratio = settings.phot_ratio;  % photons in output / all simulated photons
smoothing = settings.smoothing;
emin = settings.emin;
emax_in = settings.emax;
angle_restriction = settings.angle_restriction;

boundary_y = settings.boundary_y * 1e-6;
omit_y = [settings.by(1)+boundary_y, settings.by(2)-boundary_y];

use_tags = settings.tags;

scale_factor_cs = 1e-12;
scale_factor_bs = 1e-12;

l=GetDataSDF(filename);

el_chg = 1.6021766208e-19;

x=l.Grid.Particles.subset_phot.photon.x;
y=l.Grid.Particles.subset_phot.photon.y;
px=l.Particles.Px.subset_phot.photon.data;
py=l.Particles.Py.subset_phot.photon.data;
pz=l.Particles.Pz.subset_phot.photon.data;
ene=sqrt(px.^2+py.^2+pz.^2)*3e8/1.602e-19/1e6;  % in MeV
w=l.Particles.Weight.subset_phot.photon.data;

disp(['Number of macroparticles '  num2str(size(w,1)) ]);

if use_tags
    tag=l.Particles.Tag.subset_phot.photon.data;
end

max_ene = max(ene);
emax = min(max_ene,emax_in);
if ~isfinite(emax)
    emax = max_ene;
end
disp(['Max energy: ' num2str(emax) ' MeV'])

pulse_energy = pulse_energy_j / el_chg / 1e6; % From 'n' file, in MeV
disp(['Pulse energy: ' num2str(pulse_energy_j) ' J = ' num2str(pulse_energy, '%10.3e') ' MeV']);

nbins = max(2,floor(emax-emin)/smoothing);  % Draw at least two bins in case emax-emin < 2*smoothing
nbins = min(1000000, nbins);                  % Cap number of bins to 1 000 000 (Matlab can't ahndle "too many" bins)
disp(['nbins = ' num2str(nbins)]);
ebin_size = (emax-emin)/nbins; % approx. 1 MeV bins if not capped.

%electron_weight = 0.5*wmax*photon_multiplier;
%number_of_electrons = 625000;
%scale_factor_bs = 1/number_of_electrons/electron_weight;

if use_tags
    figure;
    hold on;
    for try_tag = 1:2
        if try_tag == 1
            rad_type = 'CS';
            rad_name = 'Inverse Compton Scattering';
            scale_factor = scale_factor_cs/smoothing;
        elseif try_tag == 2
            rad_type = 'BS';
            rad_name = 'Bremsstrahlung';
            scale_factor = scale_factor_bs/smoothing;
        end

        if any(tag==try_tag)
            if angle_restriction < 90
                idx_dir = find_in_direction(1, angle_restriction, px, py);
            else
                idx_dir = find_in_direction(0, 90, px, py);
            end
            idx_all = find(tag==try_tag & y>omit_y(1) & y<omit_y(2));
            idx = intersect(idx_dir, idx_all);

            % Weighted histogram, msg. 7 at (acumarray fails with zeros):
            % https://www.mathworks.com/matlabcentral/newsreader/view_thread/265558,
            hist_n = emin:ebin_size:emax;
            [n, edges, bin]= histcounts(ene(idx), hist_n);
            h = zeros(size(n));
            wsel = w(idx);
            npart = length(wsel);
            for i = 1:npart
                if bin(i) > 0
                    h(bin(i)) = h(bin(i)) + wsel(i);
                end
            end

            p = plot(hist_n(1:end-1), scale_factor*h, 'LineWidth', 2, 'DisplayName', rad_name);
            set(gca, 'YScale', 'log');

            total_energy = sum(ene(idx).*w(idx)) / phot_ratio;

            disp(['Total energy (' rad_type '): ' num2str(total_energy, '%10.3e')]);
            disp(['Conversion efficiency (' rad_type '):  ' num2str(total_energy/pulse_energy*100) '%']);
            disp(['Number of macroparticles (' rad_type '): ' num2str(npart) ]);
        else
            disp(['No ' rad_type ' photons']);
        end
    end
    legend('show', 'Location', 'SouthOutside', 'Orientation', 'Horizontal');
    title(['\gamma energy spectrum: ' simtitle]);
    xlabel('photon energy [MeV]');
    ylabel('number of photons [arb. u.]');
    hold off;
    drawnow;
else
    figure;
    hold on;

    rad_type = 'CS';
    rad_name = 'Inverse Compton Scattering';
    scale_factor = scale_factor_cs;

    if angle_restriction < 90
        idx_dir = find_in_direction(1, angle_restriction, px, py);
    else
        idx_dir = find_in_direction(0, 90, px, py);
    end
    idx_all = find(y>omit_y(1) & y<omit_y(2));
    idx = intersect(idx_dir, idx_all);
    
    
    % Weighted histogram, msg. 7 at (acumarray fails with zeros):
    % https://www.mathworks.com/matlabcentral/newsreader/view_thread/265558,
    hist_n = emin:ebin_size:emax;
    [n, edges, bin]= histcounts(ene(idx), hist_n);
    h = zeros(size(n));
    wsel = w(idx);
    npart = length(wsel);
    for i = 1:npart
        if bin(i) > 0
            h(bin(i)) = h(bin(i)) + wsel(i);
        end
    end

    p = plot(hist_n(1:end-1), scale_factor*h, 'LineWidth', 2, 'DisplayName', rad_name);
    set(gca, 'YScale', 'log');

    total_energy = sum(ene(idx).*w(idx)) / phot_ratio;

    disp(['Total energy (' rad_type '): ' num2str(total_energy, '%10.3e')]);
    disp(['Conversion efficiency (' rad_type '):  ' num2str(total_energy/pulse_energy*100) '%']);
    disp(['Number of macroparticles (' rad_type '): ' num2str(npart) ]);


    legend('show', 'Location', 'SouthOutside', 'Orientation', 'Horizontal');
    title(['\gamma energy spectrum: ' simtitle]);
    hold off;
    drawnow;
end

end