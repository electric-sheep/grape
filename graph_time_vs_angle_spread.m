function graph_time_vs_angle_spread(filenames, simtitle, pulse_energy_j, settings, simid)

    emin = settings.emin;
    ebin_size = settings.ebin_size;
    n_ebins = settings.n_ebins;
    emax = emin + (n_ebins-1)*ebin_size;
    all_e = [0, emin:ebin_size:emax, inf];

    export_results = settings.export_results;
    differential = settings.differential;
    cumulative = settings.cumulative;
    round_n = settings.round;
    
    combined_quads = settings.combined_quads;
    separated_quads = settings.separated_quads;
    
    phot_ratio = 1;

    display_quads = find(settings.quads);
    
    disp(['Processing simulation ' simtitle]);
    el_chg = 1.6021766208e-19;
    pulse_energy = pulse_energy_j / el_chg / 1e6; % From 'n' file, in MeV
    disp(['Pulse energy: ' num2str(pulse_energy_j) ' J = ' num2str(pulse_energy, '%10.3e') ' MeV']);

    nsteps = size(filenames,2);

    angles_cumulative = zeros(2, 4, n_ebins+1, nsteps);
    angles_differential = zeros(2, 4, n_ebins+1, nsteps);
    
    angles_cumulative_combined = zeros(2, n_ebins+1, nsteps);
    angles_differential_combined = zeros(2, n_ebins+1, nsteps);
    
    times = zeros(1, nsteps);

    n = 0;

    max_ene = zeros(2,4);
    max_ene_combined = zeros(2,1);
    
    px_old = [];
    py_old = [];
    pz_old = [];
    
    for f=filenames
        %TODO: Sort and partition by energy instead of traversing the whole
        %      array for every energy bin
        n = n + 1;

        l=GetDataSDF(f{1});

        if ~isfield(l, 'Particles')
            continue
        end
        
        px = l.Particles.Px.subset_phot.photon.data;
        py = l.Particles.Py.subset_phot.photon.data;
        pz = l.Particles.Pz.subset_phot.photon.data;
        ene = sqrt(px.^2+py.^2+pz.^2)*3e8/1.602e-19/1e6;  % in MeV

        w = l.Particles.Weight.subset_phot.photon.data;
        tag = l.Particles.Tag.subset_phot.photon.data;

        alpha=atan2(py,px)/pi*180;
        neg_ang = find(alpha<1);
        alpha(neg_ang) = alpha(neg_ang)+360;
        
        times(1,n) = l.time;

        for try_tag = 1:2
            if try_tag == 1
                if settings.cs == 0
                    continue
                end
                rad_type = 'CS';
            elseif try_tag == 2
                if settings.bs == 0
                    continue
                end
                rad_type = 'BS';
            end

            idx_tag = find(tag==try_tag);
            
            if separated_quads
                for quadrant = display_quads
                    idx_quad = find(alpha >= (quadrant-1)*90 & alpha < quadrant*90);
                    idx_all = intersect(idx_quad, idx_tag);
                    if any(idx_all)

                        max_ene_now = max(ene(idx_all));
                        disp(['t = ' num2str(l.time) ', ' rad_type ' quad ' num2str(quadrant) ': Emax = ' num2str(max_ene_now)]);
                        max_ene(try_tag, quadrant) = max(max_ene_now, max_ene(try_tag, quadrant));

                        if cumulative == 1
                            for i = 1:size(all_e,2)-1   
                                idx = intersect(find(ene>all_e(i) & ene<all_e(i+1)), idx_all);
                                angles_cumulative(try_tag, quadrant, i, n) = mean(alpha(idx));
                            end
                        end

                        if differential == 1
                            %TODO: This will wrongly remove photons with exact
                            %      same momentum...
                            pxdiff = ~ismember(px, px_old);
                            pydiff = ~ismember(py, py_old);
                            pzdiff = ~ismember(pz, pz_old);
                            iarr_dif = pxdiff | pydiff | pzdiff;
                            for i = 1:size(all_e,2)-1   
                                idx = intersect(find(ene>all_e(i) & ene<all_e(i+1) & iarr_dif), idx_all);
                                angles_differential(try_tag, quadrant, i, n) = mean(alpha(idx));
                            end
                        end
                    else
                        disp(['t = ' num2str(l.time) ', no ' rad_type ' photons in quad ' num2str(quadrant)]);
                    end
                end
            end
            
            if combined_quads
                alpha_combined = alpha;
                quadrant = 5;
                idx_combined = [];
                alpha_combined = zeros(size(alpha));
                
                for quadrant = display_quads
                    idx_quad = find(alpha >= (quadrant-1)*90 & alpha < quadrant*90);
                    idx_combined = union(idx_combined, idx_quad);
                    if quadrant == 1
                        alpha_combined(idx_quad) = alpha(idx_quad);
                    elseif quadrant == 2
                        alpha_combined(idx_quad) = 180 - alpha(idx_quad);
                    elseif quadrant == 3
                        alpha_combined(idx_quad) = alpha(idx_quad) - 180;
                    elseif quadrant == 4
                        alpha_combined(idx_quad) = 360 - alpha(idx_quad);
                    end
                end
                
                idx_all = intersect(idx_combined, idx_tag);
                
                if any(idx_all)

                    max_ene_now = max(ene(idx_all));
                    disp(['t = ' num2str(l.time) ', ' rad_type ' combined: Emax = ' num2str(max_ene_now)]);
                    max_ene_combined(try_tag) = max(max_ene_now, max_ene(try_tag));

                    if cumulative == 1
                        for i = 1:size(all_e,2)-1   
                            idx = intersect(find(ene>all_e(i) & ene<all_e(i+1)), idx_all);
                            angles_cumulative_combined(try_tag, i, n) = mean(alpha_combined(idx));
                        end
                    end

                    if differential == 1
                        %TODO: This will wrongly remove photons with exact
                        %      same momentum...
                        pxdiff = ~ismember(px, px_old);
                        pydiff = ~ismember(py, py_old);
                        pzdiff = ~ismember(pz, pz_old);
                        iarr_dif = pxdiff | pydiff | pzdiff;
                        for i = 1:size(all_e,2)-1   
                            idx = intersect(find(ene>all_e(i) & ene<all_e(i+1) & iarr_dif), idx_all);
                            angles_differential_combined(try_tag, i, n) = mean(alpha_combined(idx));
                        end
                    end
                end
                    
            end
        end

        px_old = px;
        py_old = py;
        pz_old = pz;
        
    end

    for try_tag = 1:2
        if try_tag == 1
            if settings.cs == 0
                continue
            end
            rad_type = 'CS';
        elseif try_tag == 2
            if settings.bs == 0
                continue
            end
            rad_type = 'BS';
        end

        draw_graph(try_tag, rad_type);
    end
    
    export_variables();

    
    function draw_graph(tag_number, rad_name)
        if separated_quads
            for quad = display_quads

                ncurves = size(angles_cumulative, 3);

                if cumulative == 1
                    figure;
                    hold on;
                    for i = 1:ncurves
                        mx = all_e(i+1);
                        if isinf(mx)
                            mx = max_ene(tag_number, quad);
                        end
                        rat = i/ncurves;
                        plot(times, squeeze(angles_cumulative(tag_number,quad,i,:)), ...
                            'LineWidth', 2, 'Color', [rat   0.25*(quad-1)  0.5*rat*(tag_number-1)], 'DisplayName', ...
                            [num2str(round(all_e(i),round_n)) ' - ' num2str(round(mx,round_n)) ' MeV']);
                    end
                    title([rad_name ', ' simtitle ', quad ' num2str(quad)]);
                    legend('show');
                    xlabel('Time [s]');
                    ylabel('Mean angle [deg]');
                    xlim([min(times) max(times)]);
                    ylim(90*[quad-1 quad]);
                    hold off;
                    drawnow;
                end

                if differential == 1
                    figure;
                    hold on;
                    for i = 1:ncurves
                        mx = all_e(i+1);
                        if isinf(mx)
                            mx = max_ene(tag_number, quad);
                        end
                        rat = i/ncurves;
                        plot(times, squeeze(angles_differential(tag_number,quad,i,:)), ...
                            'LineWidth', 2, 'Color', [rat   0.25*(quad-1)  0.5*rat*(tag_number-1)], 'DisplayName', ...
                            [num2str(round(all_e(i),round_n)) ' - ' num2str(round(mx,round_n)) ' MeV']);
                    end
                    title([rad_name ' differential, ' simtitle ', quad ' num2str(quad)]);
                    legend('show');
                    xlabel('Time [s]');
                    ylabel('Mean angle [deg]');
                    xlim([min(times) max(times)]);
                    ylim(90*[quad-1 quad]);
                    hold off;
                    drawnow;
                end
            end
        end
        
        if combined_quads
            ncurves = size(angles_cumulative_combined, 2);

            if cumulative == 1
                figure;
                hold on;
                for i = 1:ncurves
                    mx = all_e(i+1);
                    if isinf(mx)
                        mx = max_ene_combined(tag_number);
                    end
                    rat = i/ncurves;
                    plot(times, squeeze(angles_cumulative_combined(tag_number,i,:)), ...
                        'LineWidth', 2, 'Color', [rat   0  0.5*rat*(tag_number-1)], 'DisplayName', ...
                        [num2str(round(all_e(i),round_n)) ' - ' num2str(round(mx,round_n)) ' MeV']);
                end
                title([rad_name ', ' simtitle ', combined']);
                legend('show');
                xlabel('Time [s]');
                ylabel('Mean angle [deg]');
                xlim([min(times) max(times)]);
                ylim([0 90]);
                hold off;
                drawnow;
            end

            if differential == 1
                figure;
                hold on;
                for i = 1:ncurves
                    mx = all_e(i+1);
                    if isinf(mx)
                        mx = max_ene_combined(tag_number);
                    end
                    rat = i/ncurves;
                    plot(times, squeeze(angles_differential_combined(tag_number,i,:)), ...
                        'LineWidth', 2, 'Color', [rat   0  0.5*rat*(tag_number-1)], 'DisplayName', ...
                        [num2str(round(all_e(i),round_n)) ' - ' num2str(round(mx,round_n)) ' MeV']);
                end
                title([rad_name ' differential, ' simtitle ', combined']);
                legend('show');
                xlabel('Time [s]');
                ylabel('Mean angle [deg]');
                xlim([min(times) max(times)]);
                ylim([0 90]);
                hold off;
                drawnow;
            end
        end
        
        
    end

    function export_variables
        if export_results
            assignin('base', ['times_' num2str(simid)], times);
            if cumulative == 1
                assignin('base', ['angles_cumulative_' num2str(simid)], angles_cumulative);
            end
            if differential == 1
                assignin('base', ['angles_differential_' num2str(simid)], angles_differential);
            end
        end
    end

end

