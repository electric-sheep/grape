function absorption = get_ion_efficiency(filename, simtitle, pulse_energy_j, settings, thickness)

emin = settings.emin;
emax = settings.emax;

xmax = settings.xmax * 1e-6;
if settings.plus_thickness
    xmin = (settings.xmin + thickness) * 1e-6;
else
    xmin = settings.xmin * 1e-6;
end

ion_ratio = settings.ion_ratio;  % ions in output / all simulated photons

boundary_y = settings.boundary_y * 1e-6;
omit_y = [settings.by(1)+boundary_y, settings.by(2)-boundary_y];

angle_restriction = settings.angle_restriction;


disp(['Simulation: ' simtitle ', processing file: ' filename]);

l=GetDataSDF(filename);

el_chg = 1.6021766208e-19;

m = 49184.3*9.109e-31;

ptype = 'alum';
species = fieldnames(l.Particles.Weight.(['subset_' ptype]));

x = []; y = []; px = []; py = []; pz = []; w = [];

for i=1:numel(species)
    x = [x; l.Grid.Particles.(['subset_' ptype]).(species{i}).x];
    y = [y; l.Grid.Particles.(['subset_' ptype]).(species{i}).y];
    px = [px; l.Particles.Px.(['subset_' ptype]).(species{i}).data];
    py = [py; l.Particles.Py.(['subset_' ptype]).(species{i}).data];
    pz = [pz; l.Particles.Pz.(['subset_' ptype]).(species{i}).data];
    w = [w; l.Particles.Weight.(['subset_' ptype]).(species{i}).data];
end

ene=m*3e8^2*(1/(m*3e8)*sqrt((m*3e8)^2+px.^2+py.^2+pz.^2)-1) /el_chg/1e6;  % in MeV

pulse_energy = pulse_energy_j / el_chg / 1e6; % From 'n' file, in MeV

total_energy = zeros(2,1);
energy_in_range = zeros(2,1);
low_energy = zeros(2,1);
max_energy = zeros(2,1);
high_energy = zeros(2,1);

rad_idx = 1; %TODO: Remove this leftover form tagge photon reader

idx = find(y>omit_y(1) & y<omit_y(2) & x>xmin & x<xmax);
if angle_restriction < 90
    idx = intersect(find_in_direction(1, angle_restriction, px, py), idx);
end
if any(idx)
    max_energy(rad_idx) = max(ene(idx));

    idx_total = intersect(find(ene>=emin), idx);
    total_energy(rad_idx) = sum(ene(idx_total).*w(idx_total)) / ion_ratio;

    idx_low = intersect(find(ene<emin), idx);
    low_energy(rad_idx) = sum(ene(idx_low).*w(idx_low)) / ion_ratio;

    idx_range = intersect(find(ene<=emax), idx_total);
    energy_in_range(rad_idx) = sum(ene(idx_range).*w(idx_range)) / ion_ratio;

    idx_high = intersect(find(ene>=emax), idx_total);
    high_energy(rad_idx) = sum(ene(idx_high).*w(idx_high)) / ion_ratio;

    disp(['Pulse energy: ' num2str(pulse_energy_j) ' J = ' num2str(pulse_energy, '%10.3e') ' MeV']);

    disp(['Total energy ' ptype ' > ' num2str(emin) ' MeV: ' num2str(total_energy(rad_idx), '%10.3e') ...
          '   in ' num2str(emin) '-' num2str(emax) ' MeV: ' num2str(energy_in_range(rad_idx), '%10.3e')])

    disp(['Conversion efficiency ' ptype ' > ' num2str(emin) ' MeV:  ' num2str(total_energy(rad_idx)/pulse_energy*100) ...
          '%   in ' num2str(emin) '-' num2str(emax) ' MeV: ' num2str(energy_in_range(rad_idx)/pulse_energy*100) '%'])

    disp(['Low energy ' ptype ' < ' num2str(emin) ' MeV: '  num2str(low_energy(rad_idx), '%10.3e') ...
          ' MeV. Conversion efficiency: ' num2str(low_energy(rad_idx)/pulse_energy*100) '%']);

    disp(['High energy ' ptype ' < ' num2str(emin) ' MeV: '  num2str(high_energy(rad_idx), '%10.3e') ...
          ' MeV. Conversion efficiency: ' num2str(high_energy(rad_idx)/pulse_energy*100) '%']);

else
    disp(['No ' ptype ' ions in selected region']);
    max_energy(rad_idx) = 0;
    total_energy(rad_idx) = 0;
    low_energy(rad_idx) = 0;
    energy_in_range(rad_idx) = 0;
    high_energy(rad_idx) = 0;
end

absorption = struct(...
    'pulse_energy_j', pulse_energy_j, ...
    'pulse_energy', pulse_energy, ...
    'total_energy', total_energy, ...
    'energy_in_range', energy_in_range, ...
    'low_energy', low_energy, ...
    'high_energy', high_energy, ...
    'max_energy', max_energy);

end

