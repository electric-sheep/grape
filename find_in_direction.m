function idx = find_in_direction(direction, angle_restriction, x, y)
%FIND_IN_DIRECTION Returns indexes of elements in x half-plane
%   0 returns all elements, -1 those going to the left, +1 those going to
%   the right, and those on the axis center.

    if angle_restriction >= 90
        idx_angle = find(x);
    else
        tg = tan(pi*angle_restriction/180);
        idx_angle = find(abs(y) < tg*abs(x));
    end

    if direction == 0
        idx_direction = find(x);
    elseif direction == -1
        idx_direction = find(x < 0);
    elseif direction == 1
        idx_direction = find(x >= 0);
    else
        idx_direction = [];
    end
    
    idx = intersect(idx_angle, idx_direction);

end

